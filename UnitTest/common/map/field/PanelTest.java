package common.map.field;

import server.logic.GameLogic;
import common.map.Direction;
import common.map.entity.panel.Panel;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class PanelTest {

    @Test
    void getMeta() {
        for (Direction facingDirection : Direction.values()) {
            int meta = facingDirection.ordinal() << 6;
            assertEquals(meta, new Panel(0, new Point(), meta, new GameLogic(null)).getMeta()); //TODO: Rendes map után se jó?
        }
    }

    @Test
    void getFacingDirection() {
        for (Direction facingDirection : Direction.values()) {
            int meta = facingDirection.ordinal() << 6;
            assertEquals(facingDirection, new Panel(0, new Point(), meta, new GameLogic(null)).getFacingDirection());
        }
    }
}