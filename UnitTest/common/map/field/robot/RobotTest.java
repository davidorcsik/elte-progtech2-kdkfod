package common.map.field.robot;

import common.map.entity.robot.Robot;
import common.map.entity.robot.RobotType;
import server.logic.GameLogic;
import common.map.Direction;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class RobotTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getMeta() {
        for (RobotType type : RobotType.values()) {
            for (Direction facingDirection : Direction.values()) {
                int meta = (type.ordinal() << 6) | (facingDirection.ordinal() << 4);
                assertEquals(meta, new Robot(0, new Point(), meta, new GameLogic(null)).getMeta());
            }
        }
    }

    @Test
    void getBehaviour() {
        for (RobotType type : RobotType.values()) {
            int meta = (type.ordinal() << 6);
            assertEquals(type, RobotType.getRobotTypeFromClassName(new Robot(0, new Point(), meta, new GameLogic(null)).getBehaviour().getClass()));
        }
    }

    @Test
    void getFacingDirection() {
        for (Direction facingDirection : Direction.values()) {
            int meta = (facingDirection.ordinal() << 4);
            assertEquals(facingDirection, new Robot(0, new Point(), meta, new GameLogic(null)).getFacingDirection());
        }
    }
}