package common.map.field;

import common.map.Map;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class FieldTest {
    private Random random = new Random();

    @Test
    void getHexRepresentation() {
        for (int type = 0; type < FieldType.values().length; ++type) {
            if (FieldType.values()[type].isEntity) continue;
            for (int level = 0; level < 9; ++level) {
                int redChannel = type << 4 | level;
                int greenChannel = 0; //meta
                int blueChannel = 0; //not used
                Point location = new Point(random.nextInt(), random.nextInt());
                Field decodedField = Map.decodeFieldFromColorCode(new Color(redChannel, greenChannel, blueChannel), location,null);
                assertEquals(redChannel, decodedField.getColorRepresentation().getRed());
            }
        }
    }
}