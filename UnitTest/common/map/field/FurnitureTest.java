package common.map.field;

import common.map.entity.furniture.Furniture;
import common.map.entity.furniture.FurnitureType;
import server.logic.GameLogic;
import common.map.Direction;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class FurnitureTest {

    @Test
    void getMeta() {
        for (FurnitureType type : FurnitureType.values()) {
            for (Direction facingDirection : Direction.values()) {
                for (int width = 0; width <= 3; ++width) { //Mivel a szélesség 2 biten van tárolva ezért 4 lehetőség van
                    for (int height = 0; height <= 3; ++height) { //Mivel a magasság 2 biten van tárolva ezért 4 lehetőség van
                        int meta = (type.ordinal() << 6) | (facingDirection.ordinal() << 4) | (width << 2) | (height << 2);
                        assertEquals(meta, new Furniture(0, new Point(), meta, new GameLogic(null)).getMeta());
                    }
                }
            }
        }
    }

    @Test
    void getType() {
        for (FurnitureType type : FurnitureType.values()) {
            int meta = (type.ordinal() << 6);
            assertEquals(type, new Furniture(0, new Point(), meta, new GameLogic(null)).getType());
        }
    }

    @Test
    void getFacingDirection() {
        for (Direction facingDirection : Direction.values()) {
            int meta = (facingDirection.ordinal() << 4);
            assertEquals(facingDirection, new Furniture(0, new Point(), meta, new GameLogic(null)).getFacingDirection());
        }
    }

    @Test
    void getSize() {
        for (int width = 0; width <= 3; ++width) { //Mivel a szélesség 2 biten van tárolva ezért 4 lehetőség van
            for (int height = 0; height <= 3; ++height) { //Mivel a magasság 2 biten van tárolva ezért 4 lehetőség van
                int meta = (width << 2) | (height);
                assertEquals(new Dimension(width + 1, height + 1), new Furniture(0, new Point(), meta, new GameLogic(null)).getSize());
            }
        }
    }
}