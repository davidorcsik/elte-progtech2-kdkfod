package common.map.field;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class ElevatorTest {

    @Test
    void getMeta() {
        for (int i = 0; i < 16; ++i) { //Mivel a szint 4 biten van tárolva ezért 16 különböző érték lehetséges
            for (int j = 0; j < 2; ++j) { //Mivel a típus 1 biten van tárolva ezért 2 különböző érték lehetséges
                int meta = (i << 4) | (j << 3);
                assertEquals(meta, new Elevator(0, new Point(), meta, null).getMeta());
            }
        }
    }

    @Test
    void getDestinationLevel() {
        for (int i = 0; i < 16; ++i) { //Mivel a szint 4 biten van tárolva ezért 16 különböző érték lehetséges
            int meta = (i << 4);
            assertEquals(i, new Elevator(0, new Point(), meta, null).getDestinationLevel());
        }
    }

    @Test
    void isDestination() {
        for (int i = 0; i < 2; ++i) { //Mivel a típus 1 biten van tárolva ezért 2 különböző érték lehetséges
            int meta = (i << 3);
            assertEquals(meta, new Elevator(0, new Point(), meta, null).getMeta());
        }
    }
}