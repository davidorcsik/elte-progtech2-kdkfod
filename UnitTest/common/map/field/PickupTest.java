package common.map.field;

import common.map.entity.pickup.Pickup;
import common.map.entity.pickup.PickupType;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

class PickupTest {

    @Test
    void getMeta() {
        for (PickupType type : PickupType.values()) {
            int meta = type.ordinal() << 7;
            assertEquals(meta, new Pickup(0, new Point(), meta, null).getMeta());
        }
    }

    @Test
    void getType() {
        for (PickupType type : PickupType.values()) {
            int meta = type.ordinal() << 7;
            assertEquals(type, new Pickup(0, new Point(), meta, null).getType());
        }
    }
}