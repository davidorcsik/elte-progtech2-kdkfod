package common.map;

import common.map.field.Field;
import common.map.field.FieldType;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.Arrays;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class MapTest {
    private Random random = new Random();

    @Test
    void decodeFieldFromColorCode() {
        for (int type = 0; type < FieldType.values().length; ++type) {
            if (FieldType.values()[type].isEntity) continue;
            for (int level = 0; level < 9; ++level) {
                int redChannel = type << 4 | level;
                int greenChannel = 0; //meta
                int blueChannel = 0; //not used
                Point location = new Point(random.nextInt(), random.nextInt());
                Field decodedField = Map.decodeFieldFromColorCode(new Color(redChannel, greenChannel, blueChannel), location, null);
                assertEquals(FieldType.values()[type].associatedClassName, decodedField.getClass());
                assertEquals(level, decodedField.getAssociatedLevel());
                assertEquals(location, decodedField.getLocation());
            }
        }
    }
}