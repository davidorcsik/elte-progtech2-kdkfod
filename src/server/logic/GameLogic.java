package server.logic;

import common.map.Direction;
import common.map.Map;
import common.map.entity.Actor;
import common.map.entity.Entity;
import common.map.entity.Projectile;
import common.map.entity.furniture.Furniture;
import common.map.entity.panel.Panel;
import common.map.entity.pickup.Pickup;
import common.map.entity.player.InventoryItem;
import common.map.entity.player.Player;
import common.map.entity.robot.Robot;
import common.map.field.*;
import common.utility.Message;
import common.utility.MessageType;
import common.utility.Utility;
import server.logic.worker.Worker;

import java.awt.*;
import java.io.Closeable;
import java.util.Queue;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GameLogic implements Runnable, Closeable {
    public final static long GAME_TICK = 100;
    private final static long GAME_START_DELAY = 250;
    private final static long MAP_CHANGE_DELAY = 2000;
    public final static long GAME_TIME = 1440; //in seconds

    private static long WORKER_COUNT = 1;

    private final Set<Worker> associatedWorkers = new HashSet<>();
    private final GameServer gameServer;
    private final ScheduledExecutorService gameTimer = Executors.newSingleThreadScheduledExecutor();
    private final ScheduledExecutorService mapChangeTimer = Executors.newSingleThreadScheduledExecutor();
    private final Queue<Message> stateChanges = new LinkedList<>();
    private final String deactivationCode;

    private Timer timer = new Timer();
    private long timeLeft = GAME_TIME * 1000;
    private Map map;
    private Map differenceMap;
    private boolean gameStarted = false;

    public GameLogic(GameServer gameServer) {
        this.gameServer = gameServer;
        this.deactivationCode = gameServer.getRandomWord();
        map = Map.createMapFromImage("lvl01.bmp", this);
        map.setLevel(1);
        map.createRandomFurnituresWithLetter(deactivationCode);
        for(Robot r : map.getRobots()) r.getBehaviour().createPatrolFields();
    }

    boolean isFull() {
        return associatedWorkers.size() >= 2;
    }

    void addWorker(Worker worker) {
        if (worker == null) throw new IllegalArgumentException("Worker cannot be null!");
        if (isFull()) throw new IllegalStateException("Game is already full!");

        associatedWorkers.add(worker);
        if (isFull()) startGame();
    }

    Set<Worker> getWorkers() {
        return associatedWorkers;
    }

    @Override
    public void close() {
        gameTimer.shutdownNow();
        timeLeft = 0;
        GameLogic _this = this;
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("CLOSE");
                stateChanges.clear();
                associatedWorkers.forEach(Utility::closeClosable);
                associatedWorkers.clear();
                gameServer.closeGame(_this);
            }
        }, 1000);
    }

    public Map getMap() { return map; }

    @Override
    public void run() {
        if (gameOver()) {
            associatedWorkers.forEach(Worker::messageTimeOut);
            close();
            return;
        }
        timeLeft -= 1;
        try {
            Map preModificationMapHardCopy = (Map) Utility.copyObject(map);
            if(map.getProjectiles()!=null) moveProjectiles();
            decreaseOverfilledHealthPoints();
            unleashTheHell();
            getUpdatesFromWorkers();
            getUpdatesFromRobots();
            doPatrol();
            Player[] players = map.getPlayers();
            Message currentStateChange;
            while((currentStateChange=stateChanges.poll())!=null) {
                Actor actor = (Actor) map.getEntityWithUID(currentStateChange.getUid());
                if (currentStateChange.getType().equals(MessageType.STATE_CHANGE) && actor.isAlive()) {
                    switch (currentStateChange.getAction()) {
                        case MOVE:
                            move(actor, currentStateChange);
                            break;
                        case INTERACT:
                            interact((Player) actor);
                            break;
                        case ATTACK:
                            shootProjectile(actor);
                            break;
                        case DEACTIVATION_CODE:
                            handleDeactivationAttempt(currentStateChange.getDeactivationCode());
                            break;
                    }
                    isPlayerStandOnElevator(actor);
                }

                if(actor.getClass().equals(Player.class)) System.out.println(actor.toString());
            }
            respawnDeadPlayers();
            differenceMap = Map.getDifferenceMap(preModificationMapHardCopy, map);
            messageGameTickToEntities();
        } catch (Exception e) { e.printStackTrace(); }
    }

    private void respawnDeadPlayers() {
        for (Player player : map.getPlayers()) {
            if (!player.isAlive()) {
                Iterator stateChangeIterator = stateChanges.iterator();
                while (stateChangeIterator.hasNext()) {
                    Message currentStateChange = (Message) stateChangeIterator.next();
                    if (currentStateChange.getUid() == player.getId()) stateChangeIterator.remove();
                }
                respawn(player);
            }
        }
    }

    private void doPatrol() {
        for(Robot r : map.getRobots()) r.getBehaviour().patrolMode();
    }

    private void handleDeactivationAttempt(String providedDeactivationCode) {
        if (deactivationCode.equals(providedDeactivationCode)) win();
    }

    private void win() {
        associatedWorkers.forEach(Worker::messageWin);
        timer.cancel();
        close();
    }

    private void updateActorDirection(Actor actor, Point displacement) {
        actor.setFacingDirection(Direction.getDirectionFromDisplacement(displacement.x, displacement.y));
    }

    private void respawn(Actor actor) {
        Point death = actor.getLocation();
        map.getField(death.x, death.y).setContainedEntity(null);
        Point currentRespawn = map.getNextEmptySpawnFieldLocation();
        ((Player) actor).respawn(currentRespawn);
        map.getField(actor.getLocation().x, actor.getLocation().y).setContainedEntity((Entity) actor);
    }

    private boolean bothPlayerInsidePanelRoom() {
        Player[] players = map.getPlayers();
        Point[] panelRoomCorners = {new Point(45, 27), new Point(73, 57)};
        return ((panelRoomCorners[0].x <= players[0].getLocation().x && players[0].getLocation().x <= panelRoomCorners[1].x) && (panelRoomCorners[0].y <= players[0].getLocation().y && players[0].getLocation().y <= panelRoomCorners[1].y))
        && ((panelRoomCorners[0].x <= players[1].getLocation().x && players[1].getLocation().x <= panelRoomCorners[1].x) && (panelRoomCorners[0].y <= players[1].getLocation().y && players[1].getLocation().y <= panelRoomCorners[1].y));
    }

    private void unleashTheHell() {
        if(map.getLevel()==9 && canPlayersMoveToNextLevel() && bothPlayerInsidePanelRoom() && map.getField(44, 31).getFieldType().equals(FieldType.WALL)) {
            for(int x=44;x<=74;x+=30){
                for(int y=31;y<40;y++){
                    map.setField(x, y, new Ground(9, new Point(x,y), 0, this));
                }
            }
            for(int x=47;x<72;x++) map.setField(x, 26, new Ground(9, new Point(x, 26), 0, this));
            for(int x=43;x<=75;x+=32) map.setField(x, 50, new Wall(9, new Point(x, 50), 0, this));
            for(int x=44;x<=74;x+=30) map.setField(x, 50, new Spawn(9, new Point(x, 50), 0, this));
            for(int x=51;x<=66;x+=15) map.setField(x, 160, new Ground(9, new Point(x, 160), 0, this));
            for(Robot r : map.getRobots()) {
                r.getBehaviour().setAlarmDistance(50);
                r.getBehaviour().createPatrolFields();
            }
        }
    }

    private void shootProjectile(Actor actor) {
        if(actor.getAmmoAmount()>0) {
            Point nextField = Utility.getPointInDirection(new Point(actor.getLocation().x, actor.getLocation().y), actor.getFacingDirection());
            updateActorDirection(actor, Direction.getDisplacementFromPoints(actor.getLocation(), nextField));
            if(map.getField(nextField.x, nextField.y).isStepable()) {
                Projectile projectile = new Projectile(nextField, this, actor.getFacingDirection(), actor);
                map.getField(projectile.getLocation().x, projectile.getLocation().y).setContainedEntity(projectile);
                map.addEntity(projectile);
                if(actor.getClass().equals(Player.class)) ((Player) actor).shoot();
            } else if(map.getField(nextField.x, nextField.y).getContainedEntity().getClass().equals(Robot.class)
                   || map.getField(nextField.x, nextField.y).getContainedEntity().getClass().equals(Player.class)) {
                Projectile projectile = new Projectile(actor.getLocation(), this, actor.getFacingDirection(), actor);
                if(map.getField(nextField.x, nextField.y).getContainedEntity().getClass().equals(projectile.getTargetEntityType())) {
                    if(actor.getClass().equals(Player.class)) ((Player) actor).shoot();
                    ((Actor)map.getField(nextField.x, nextField.y).getContainedEntity()).damage(projectile.getDamageValue());
                }
            }
        }
    }

    private void moveProjectiles() {
        Iterator<Projectile> i = map.getProjectiles().iterator();
        while (i.hasNext()) {
            Projectile projectile = i.next();
            Point nextField = projectile.getNextField();
            if(map.getField(nextField.x, nextField.y).getFieldType().equals(FieldType.GROUND) && map.getField(nextField.x, nextField.y).getContainedEntity() == null) {
                map.getField(projectile.getLocation().x, projectile.getLocation().y).setContainedEntity(null);
                map.getField(nextField.x, nextField.y).setContainedEntity(projectile);
                projectile.move();
            } else if(map.getField(nextField.x, nextField.y).getContainedEntity() != null) {
                if(map.getField(nextField.x, nextField.y).getContainedEntity().getClass().equals(projectile.getTargetEntityType())) {
                    ((Actor)map.getField(nextField.x, nextField.y).getContainedEntity()).damage(projectile.getDamageValue());
                }
                i.remove();
                map.getField(projectile.getLocation().x, projectile.getLocation().y).setContainedEntity(null);
            } else {
                i.remove();
                map.getField(projectile.getLocation().x, projectile.getLocation().y).setContainedEntity(null);
            }
        }
    }

    private void decreaseOverfilledHealthPoints() {
        for(Player p : map.getPlayers()) {
            if(p.getHealthPoint()>p.MAX_HEALTH) p.damage(1);
        }
    }

    private void messageGameTickToEntities() {
        try { associatedWorkers.forEach(Worker::gameTick); }
        catch (Exception ignored) {}
    }

    private void getUpdatesFromWorkers() {
        associatedWorkers.forEach(w -> stateChanges.addAll(w.getStateChanges()));
    }
    private void getUpdatesFromRobots() {
        map.getRobots().forEach(r -> stateChanges.addAll(r.getStateChanges()));
    }

    private void move(Actor actor, Message currentStateChange) {
        //actor.damage(40); //DEBUG (and pain)
        Point newPosition = currentStateChange.getNewPosition();
        Point currentPosition = actor.getLocation();
        updateActorDirection(actor, Direction.getDisplacementFromPoints(currentPosition, newPosition));
        if(map.getField(newPosition.x, newPosition.y).isStepable()) {
            if(actor.getClass().equals(Player.class)) moveToPickupIfPossible(newPosition, (Player) actor);
            map.getField(currentPosition.x, currentPosition.y).setContainedEntity(null);
            map.getField(newPosition.x, newPosition.y).setContainedEntity((Entity) actor);
            actor.setLocation(newPosition);
        }
    }

    private ArrayList<Entity> getEntitiesFromInteractArea(Point playerPosition) {
        ArrayList<Entity> reachableEntities = new ArrayList<>();
        for(int i=-1;i<=1;++i){
            for(int j=-1;j<=1;++j){
                Entity interactableEntity = map.getField(playerPosition.x + i, playerPosition.y + j).getContainedEntity();
                if(Math.abs(i+j)==1 && (interactableEntity != null)
                        && (interactableEntity.getClass().equals(Furniture.class) || interactableEntity.getClass().equals(Panel.class) || interactableEntity.getClass().equals(Player.class))) {
                    reachableEntities.add(map.getField(playerPosition.x+i, playerPosition.y+j).getContainedEntity());
                }
            }
        }
        reachableEntities = sortByEntityHierarchy(reachableEntities);
        return reachableEntities;
    }

    private ArrayList<Entity> sortByEntityHierarchy(ArrayList<Entity> list) {
        Object[] hierarchy = {Player.class, Furniture.class, Panel.class};
        int index = 0;
        for(int i=0;i<hierarchy.length;++i){
            for(int j=index;j<list.size();++j){
                if(list.get(j).getClass().equals(hierarchy[i])) {
                    Entity entity = list.get(index);
                    list.set(index, list.get(j));
                    list.set(j, entity);
                    index++;
                }
            }
        }
        return list;
    }

    private void interact(Player actor) {
        ArrayList<Entity> interactableEntities = getEntitiesFromInteractArea(actor.getLocation());
        if(interactableEntities.size() > 0) {
            Entity interactableEntity = interactableEntities.get(0);
            if(interactableEntity.getClass().equals(Player.class)) interactWithPlayer();
            else if(interactableEntity.getClass().equals(Furniture.class)) interactWithFurniture((Furniture)interactableEntity, actor);
        }
    }

    private void interactWithPlayer() {
        LinkedList<InventoryItem> sharedInventory = new LinkedList<>();
        for (Player player : map.getPlayers()) {
            player.setMove(false);
            player.getInventory().stream().filter(item -> !sharedInventory.contains(item)).forEach(sharedInventory::add);
        }
        Arrays.stream(map.getPlayers()).forEach(p -> { p.setInventory(sharedInventory); p.setMove(true); });
    }

    private void interactWithFurniture(Furniture furniture, Player player) { //TODO: create letters
        if(furniture.hasLetter()) {
            player.appendToInventory(new InventoryItem(furniture.getContainedLetter(), map.getLevel(), furniture.getId()));
            furniture.setContainedLetter(null);
        }
        map.getField(furniture.getLocation().x, furniture.getLocation().y).setContainedEntity(null);
    }

    private boolean gameOver() {
        return timeLeft <= 0;
    }

    private void moveToPickupIfPossible(Point targetFieldPosition, Player actor) {
        Field targetField = map.getField(targetFieldPosition.x,targetFieldPosition.y);
        if(targetField.getContainedEntity() != null && targetField.getContainedEntity().getClass().equals(Pickup.class)) {
            actor.pickUp((Pickup)targetField.getContainedEntity());
            targetField.setContainedEntity(null);
        }
    }

    private boolean canPlayersMoveToNextLevel() {
        return map.getFurnitures().stream().noneMatch(Furniture::hasLetter);
    }

    private boolean isAllPlayersStandOnElevator() {
        return Arrays.stream(map.getFields(Elevator.class)).filter(e -> !((Elevator)e).isDestination()).allMatch(e -> e.getContainedEntity() != null);
    }

    private void isPlayerStandOnElevator(Actor actor) {
        if(map.getField(actor.getLocation().x, actor.getLocation().y).getClass().equals(Elevator.class)
                && !((Elevator)map.getField(actor.getLocation().x, actor.getLocation().y)).isDestination()) {
            if(canPlayersMoveToNextLevel() && isAllPlayersStandOnElevator()) changeLevel((Elevator)map.getField(actor.getLocation().x, actor.getLocation().y));
        }
    }

    private void changeLevel(Elevator senderElevator) {
        Map nextLvl = Map.createMapFromImage("lvl0"+(map.getLevel() + 1)+".bmp", this );
        nextLvl.setLevel(map.getLevel() + 1);
        nextLvl.createRandomFurnituresWithLetter(deactivationCode);
        for (Player player : map.getPlayers()) {
            nextLvl.addPlayer(player);
            Point spawnLocation = nextLvl.getNextEmptySpawnFieldLocation();
            Point elevatorLocation = nextLvl.getNextEmptyArrivalElevatorPosition();
            nextLvl.getField(elevatorLocation).setContainedEntity(player);
            player.setSpawnLocation(spawnLocation);
            player.setLocation(elevatorLocation);
        }
        map = Map.calculateNewStateFromDifferenceMap(map, nextLvl);
        map.getRobots().forEach(r -> r.getBehaviour().createPatrolFields());
        stateChanges.clear();
    }

    private void startGameTimer() {
        gameTimer.scheduleAtFixedRate(this, GAME_START_DELAY, GAME_TICK, TimeUnit.MILLISECONDS);
    }

    private void startGame() {
        gameStarted = true;
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                associatedWorkers.forEach(Worker::startGame);
            }
        }, 100);
        startGameTimer();
    }

    public synchronized static long getNextWorkerId() { return WORKER_COUNT++; }

    public Map getDifferenceMap() {
        return differenceMap;
    }

    public String getDeactivationCode() { return deactivationCode; }

    public long getTimeLeft() { return timeLeft; }

    public boolean isGameStarted() {
        return gameStarted;
    }
}
