package server.logic;

import common.utility.FilePathConstants;
import common.utility.Utility;
import server.logic.worker.Worker;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GameServer extends Thread implements Closeable {
    private final int LISTENING_PORT = 5000;
    private final Random random = new Random();
    private ServerSocket serverSocket;
    private boolean listening = true;
    private final List<Worker> workers = new LinkedList<>();
    private final List<GameLogic> games = new LinkedList<>();
    private String[] words;

    public GameServer() {
        try {
            serverSocket = new ServerSocket(LISTENING_PORT);
            serverSocket.setSoTimeout(500);
            readWordList();
        }
        catch (IOException e) { Utility.logError("Cannot create server socket!", e); }
    }

    private void readWordList() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(FilePathConstants.RESOURCE_PATH + "words.dat"));
            words = reader.lines().toArray(String[]::new);
        } catch (FileNotFoundException e) {
            words = new String[]{"ABCDEFGHI", "KEKISTANI"};
            e.printStackTrace();
        }
    }

    public String getRandomWord() {
        return words[random.nextInt(words.length)];
    }

    private void setGameForNewWorker(Worker worker) {
        GameLogic game;
        synchronized (games) {
            if (games.isEmpty() || games.get(games.size() - 1).isFull()) {
                game = new GameLogic(this);
                games.add(game);
            } else game = games.get(games.size() - 1);
            worker.setGameLogic(game);
            game.addWorker(worker);
        }
    }

    private void waitForConnections() {
        while (listening) {
            try {
                Socket socket = serverSocket.accept();
                Worker worker = new Worker(socket, this);
                workers.add(worker);
                setGameForNewWorker(worker);
                worker.start();
            } catch (IOException ignored) {} //timeout
        }
    }

    @Override
    public void run() {
        waitForConnections();
    }

    void closeGame(GameLogic gameLogic) {
        synchronized (games) { games.remove(gameLogic); }
        for (Worker w : gameLogic.getWorkers()) workers.remove(w);
    }

    @Override
    public void close() {
        listening = false;
        Utility.closeClosable(serverSocket);
        games.forEach(Utility::closeClosable);
        workers.forEach(Utility::closeClosable);
        games.clear();
        workers.clear();
    }

    public void workerLeft(Worker worker) {
        int i = 0;
        while (i < games.size() && !games.get(i).getWorkers().contains(worker)) ++i;
        if (i == games.size()) return;

        games.get(i).getWorkers().remove(worker);
        if (games.get(i).getWorkers().isEmpty()) games.remove(games.get(i));
        try { worker.join(); }
        catch (InterruptedException ignored) {}
        workers.remove(worker);
    }
}
