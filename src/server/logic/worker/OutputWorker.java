package server.logic.worker;

import java.io.IOException;

public class OutputWorker implements Runnable {
    private final Worker associatedWorker;

    OutputWorker(Worker associatedWorker) { this.associatedWorker = associatedWorker; }

    @Override
    public void run() {
        try {
            if (!associatedWorker.isWorking()) associatedWorker.close();
            synchronized (associatedWorker.outgoingMessages) {
                while (!associatedWorker.outgoingMessages.isEmpty()) {
                    associatedWorker.sendMessageNow(associatedWorker.outgoingMessages.poll());
                }
            }
        } catch (IOException e) {
            associatedWorker.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
