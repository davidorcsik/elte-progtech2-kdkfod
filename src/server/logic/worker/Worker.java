package server.logic.worker;

import common.map.Map;
import common.map.entity.player.Player;
import common.utility.Message;
import common.utility.MessageType;
import common.utility.Utility;
import server.logic.GameLogic;
import server.logic.GameServer;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Worker extends Thread implements Closeable {
    private final Object MONITOR_OBJECT = new Object();
    private final long uid;
    private final Socket socket;
    private final ObjectInputStream socketReader;
    private final ObjectOutputStream socketWriter;

    final Queue<Message<?>> incomingMessages = new LinkedList<>();
    final Queue<Message<?>> outgoingMessages = new LinkedList<>();

    private final GameServer gameServer;
    private GameLogic gameLogic;
    private InputWorker inputWorker = new InputWorker(this);
    private OutputWorker outputWorker = new OutputWorker(this);
    private ScheduledExecutorService outputMessageScheduler = Executors.newScheduledThreadPool(1);
    private boolean working = false;
    private Player player;

    public Worker(Socket socket, GameServer gameServer) throws IOException {
        this.gameServer = gameServer;
        this.uid = GameLogic.getNextWorkerId();
        this.socket = socket;
        this.socketWriter = new ObjectOutputStream(socket.getOutputStream());
        this.socketReader = new ObjectInputStream(socket.getInputStream());
    }

    public void setGameLogic(GameLogic gameLogic) {
        if (gameLogic == null) throw new IllegalArgumentException("Gamelogic cannot be null!");
        if (this.gameLogic != null) return;
        this.gameLogic = gameLogic;
        this.player = this.gameLogic.getMap().createPlayer(uid);
        this.gameLogic.getMap().addPlayer(player);
    }

    private void sendMessage(Message message) {
        synchronized (outgoingMessages) { outgoingMessages.add(message); }
    }

    void sendMessageNow(Message message) throws IOException {
        synchronized (socketWriter) {
            socketWriter.writeObject(message);
            socketWriter.reset();
        }
    }

    Message<?> readMessage() throws IOException, ClassNotFoundException { synchronized(socketReader) { return (Message<?>) socketReader.readObject(); }}

    @Override
    public void run() {
        System.out.println("Client connected");
        working = true;
        inputWorker.start();
        outputMessageScheduler.scheduleAtFixedRate(outputWorker, 0, GameLogic.GAME_TICK / 2, TimeUnit.MILLISECONDS);
        sendMessage(new Message<>(MessageType.HANDSHAKE, new long[]{uid, GameLogic.GAME_TIME}));
        synchronized (MONITOR_OBJECT) {
            try { MONITOR_OBJECT.wait(); }
            catch (InterruptedException ignored) {}
        }
    }

    public boolean isWorking() { return working && socket.isBound(); }

    public long getUid() { return this.uid; }

    @Override
    public void close() {
        if (gameLogic == null) return;
        gameLogic = null;
        working = false;
        Utility.logNotice("Worker " + uid + " stopping");
        synchronized (MONITOR_OBJECT) { MONITOR_OBJECT.notify(); }
        outputMessageScheduler.shutdown();
        Utility.closeSocket(socket);
        Utility.closeClosable(socketReader);
        Utility.closeClosable(socketWriter);
        if (gameLogic != null && gameLogic.isGameStarted()) Utility.closeClosable(gameLogic);
        inputWorker = null;
        outputWorker = null;
    }

    public void startGame() {
        sendMessage(new Message<>(MessageType.START, gameLogic.getMap()));
    }

    public LinkedList<Message> getStateChanges() {
        LinkedList<Message> temp = new LinkedList<>();
        Message currentMessage;
        while ((currentMessage = incomingMessages.poll()) != null) temp.add(currentMessage);
        return temp;
    }

    public Player getPlayer() { return this.player; }

    public void gameTick() {
        sendMessage(new Message<>(MessageType.STATE_CHANGE, new Map(gameLogic.getDifferenceMap())));
    }

    public void messageWin() {
        sendMessage(new Message<>(MessageType.WIN, null));
    }

    public void messageTimeOut() {
        sendMessage(new Message<>(MessageType.GAME_OVER, null));
    }

    void lobbyLeft() {
        close();
        gameServer.workerLeft(this);
    }
}
