package server.logic.worker;

import common.utility.Message;
import common.utility.MessageType;

import java.io.IOException;

public class InputWorker extends Thread {
    private final Worker associatedWorker;

    InputWorker(Worker associatedWorker) {
        this.associatedWorker = associatedWorker;
    }

    @Override
    public void run() {
        Message<?> receivedMessage;
        try {
            while ((receivedMessage = associatedWorker.readMessage()) != null) {
                synchronized (associatedWorker.incomingMessages) {
                    if (receivedMessage.getType().equals(MessageType.ABORT)) associatedWorker.lobbyLeft();
                    else associatedWorker.incomingMessages.add(receivedMessage);
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            associatedWorker.close();
        }
    }
}
