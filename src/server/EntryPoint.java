package server;

import server.logic.GameServer;

public class EntryPoint {
    public static void main(String[] args) {
        GameServer gameServer = new GameServer();
        gameServer.start();
        try {
            gameServer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
