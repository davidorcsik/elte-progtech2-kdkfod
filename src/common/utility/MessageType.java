package common.utility;

public enum MessageType {
    HANDSHAKE, START, GAME_OVER, STATE_CHANGE, ENTITY_ACTION, WIN, ABORT
}
