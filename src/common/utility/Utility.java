package common.utility;

import client.utility.ImageLoader;
import common.map.Direction;
import common.map.exception.FieldParseException;
import common.map.field.FieldType;
import server.logic.GameLogic;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.Socket;

public class Utility {
    public static Color[][] getRGBMatrixFromImage(String inputImagePath) {
        BufferedImage inputImage = (BufferedImage) ImageLoader.getResourceImage(inputImagePath);
        Color[][] imageRGBMatrix = new Color[inputImage.getWidth()][inputImage.getHeight()];
        for (int i = 0; i<imageRGBMatrix.length; ++i) {
            for (int j = 0; j<imageRGBMatrix[0].length; ++j) imageRGBMatrix[i][j] = new Color(inputImage.getRGB(i, j));
        }
        return imageRGBMatrix;
    }

    private static void log(String message) {synchronized (System.out) { System.out.println(message); }};

    public static void logError(String message) { log("Error" + message); }

    public static void logError(String message, Exception e) { logError(message + "\n" + e.getMessage()); }

    public static void logNotice(String message) { log("Notice: " + message); }

    public static void closeClosable(AutoCloseable closeable) {
        if (closeable == null) return;
        try { closeable.close(); }
        catch (Exception ignored) { }
    }

    public static void closeSocket(Socket socket) {
        if (socket == null) return;
        try {socket.shutdownInput();}
        catch (IOException ignored) {}
        try {socket.shutdownOutput();}
        catch (IOException ignored) {}
        closeClosable(socket);
    }

    public static Object createMapSegmentByFieldType(FieldType fieldType, int level, int meta, Point location, GameLogic logic) {
        try {
            return fieldType.associatedClassName.getConstructor(Integer.class, Point.class, Integer.class, GameLogic.class)
                    .newInstance(level, location, meta, logic);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new FieldParseException("Given input does not represent a field!");
        }
    }

    public static Object copyObject(Object object) {
        try {
            return object.getClass().getConstructor(object.getClass()).newInstance(object);
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException ignored) {}
        return null;
    }

    public static Point getPointInDirection(Point origin, Direction direction) {
        return new Point(origin.x + direction.getDx(), origin.y + direction.getDy());
    }
}
