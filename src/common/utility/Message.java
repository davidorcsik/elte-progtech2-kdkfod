package common.utility;

import common.map.entity.player.ActionType;

import java.awt.*;
import java.io.Serializable;
import java.util.AbstractMap;

public class Message<T extends Serializable> implements Serializable {
    private final MessageType type;
    private final T content;
    private final long uid;

    public Message(MessageType type, T content, long uid) {
        this.type = type;
        this.content = content;
        this.uid = uid;
    }

    public Message(MessageType type, T content) {
        this(type, content, 0);
    }

    public static Message createMessageFromIntent(Point newPosition, ActionType interact, long uid) {
        return new Message<>(MessageType.STATE_CHANGE, new AbstractMap.SimpleEntry<>(newPosition, interact), uid);
    }

    public static Message createDeactivationCodeMessage(long uid, String deactivationCode) {
        return new Message<>(MessageType.STATE_CHANGE, new AbstractMap.SimpleEntry<>(deactivationCode, ActionType.DEACTIVATION_CODE), uid);
    }

    public Point getNewPosition() {
        if (!(content instanceof AbstractMap.SimpleEntry)) throw new IllegalStateException("Message is not an abstract map!");
        return ((AbstractMap.SimpleEntry<Point, ActionType>) content).getKey();
    }

    public ActionType getAction() {
        if (!(content instanceof AbstractMap.SimpleEntry)) throw new IllegalStateException("Message is not an abstract map!");
        return ((AbstractMap.SimpleEntry<Point, ActionType>) content).getValue();
    }

    public String getDeactivationCode() {
        if (!(content instanceof AbstractMap.SimpleEntry)) throw new IllegalStateException("Message is not an abstract map!");
        return ((AbstractMap.SimpleEntry<String, ActionType>) content).getKey();
    }

    public MessageType getType() { return type; }

    public T getContent() { return content; }

    public long getUid() {
        return uid;
    }

    @Override
    public String toString() {
        return "type: " + type.name() + ", content: " + content.toString() + ", uid: " + uid + ", action_type: " + getAction() + ", position: " + getNewPosition();
    }
}
