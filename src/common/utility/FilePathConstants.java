package common.utility;

public class FilePathConstants {
    public static final String RESOURCE_PATH = "Resources/";
    public static final String IMAGES_PATH = RESOURCE_PATH + "images/";
    public static final String MAPS_PATH = RESOURCE_PATH + "maps/";
    public static final String SPRITES_PATH = RESOURCE_PATH + "sprites/";
    public static final String INVENTORY_LETTER_PATH = IMAGES_PATH + "letters/";
}
