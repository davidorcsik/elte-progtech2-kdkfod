package common.map;

import common.map.entity.Entity;
import common.map.entity.Projectile;
import common.map.entity.furniture.Furniture;
import common.map.entity.pickup.Pickup;
import common.map.entity.player.Player;
import common.map.entity.robot.Robot;
import common.map.field.*;
import common.utility.FilePathConstants;
import common.utility.Utility;
import server.logic.GameLogic;

import java.awt.*;
import java.io.Serializable;
import java.util.List;
import java.util.*;

public class Map implements Serializable {
    private final long UID_OFFSET = 1000;
    private long currentUID = UID_OFFSET;
    private final Field[][] fields;
    private final transient GameLogic logic;
    private final int MAX_LETTER_NUMBER = 4;
    private int level;
    private Player[] players = new Player[2];
    private HashMap<Class<? extends Entity>, List<Entity>> entities = new HashMap<>();
    private List<Character> letters = new ArrayList<>();

    private Map(Color[][] rawMapData, GameLogic logic) {
        fields = new Field[rawMapData.length][rawMapData[0].length];
        for (int i = 0; i<rawMapData.length; ++i) {
            for (int j = 0; j<rawMapData[i].length; ++j) {
                fields[i][j] = decodeFieldFromColorCode(rawMapData[i][j], new Point(i, j), logic);
                if (fields[i][j].getContainedEntity() != null) {
                    fields[i][j].getContainedEntity().setId(getNextUID());
                    addEntity(fields[i][j].getContainedEntity());
                }
            }
        }
        this.logic = logic;
    }

    private Map(Field[][] fields) { this(fields, null); }

    private Map(Field[][] fields, GameLogic logic) {
        this.fields = fields;
        this.logic = logic;
    }

    public Map(Map other) {
        this.currentUID = other.currentUID;
        this.fields = new Field[other.fields.length][other.fields[0].length];
        this.players = new Player[other.players.length];
        for (int i = 0; i<other.players.length; ++i) this.players[i] = new Player(other.players[i]);
        this.logic = other.logic; //Nem másolható a socketek miatt!
        this.letters = new ArrayList<>(other.letters);
        for (int i = 0; i<this.fields.length; ++i) {
            for (int j = 0; j<this.fields[i].length; ++j) {
                this.fields[i][j] = (Field) Utility.copyObject(other.fields[i][j]);
            }
        }
    }

    public void addEntity(Entity entity) {
        if (!entities.containsKey(entity.getClass())) entities.put(entity.getClass(), new ArrayList<>());
        entities.get(entity.getClass()).add(entity);
    }

    public void removeEntity(Entity entity) {
        getField(entity.getLocation()).setContainedEntity(null);
        entities.get(entity.getClass()).remove(entity);
    }

    public int getLevel() { return level; }

    public void setLevel(int level) { this.level = level; }

    private long getNextUID() {
        return ++currentUID;
    }

    public void addPlayer(Player player) {
        int i = 0;
        while (i < players.length && players[i] != null) ++i;
        players[i] = player;
        addEntity(player);
    }

    public Player createPlayer(long uid) {
        int i=0, j=0;
        while(i<fields.length && (!fields[i][j].getClass().equals(Spawn.class) || fields[i][j].getContainedEntity() != null)) {
            if(j==fields[i].length-1) {
                ++i;
                j=0;
            } else {
                ++j;
            }
        }
        Player createdPlayer = new Player(0,new Point(i,j),0,logic);
        createdPlayer.setId(uid);
        fields[i][j].setContainedEntity(createdPlayer);
        return createdPlayer;
    }

    public Player getPlayerWithID(long uid) {
        return Arrays.stream(players).filter(p -> p.getId() == uid).findFirst().orElse(null);
    }

    public Player[] getPlayers() { return this.players; }

    public List<Furniture>  getFurnitures() { return (List<Furniture>)(List<?>)entities.get(Furniture.class); }
    public List<Robot>      getRobots() { return (List<Robot>)(List<?>)entities.get(Robot.class); }
    public List<Pickup>     getPickups() { return (List<Pickup>)(List<?>)entities.get(Pickup.class);}
    public List<Projectile> getProjectiles() { return (List<Projectile>)(List<?>)entities.get(Projectile.class);}

    public List<Entity> getEntities() {
        List<Entity> allEntity = new LinkedList<>();
        for (List<Entity> entityList : entities.values()) allEntity.addAll(entityList);
        return new ArrayList<>(allEntity);
    }

    public List<Entity> getEntities(Class<? extends Entity> entityClass) {
        return entities.get(entityClass);
    }

    public void createRandomFurnituresWithLetter(String codeword) {
        List<Furniture> furnitures = getFurnitures();
        Collections.shuffle(furnitures);
        for(int i=0;i<MAX_LETTER_NUMBER;i++) furnitures.get(i).setContainedLetter(codeword.charAt(level - 1));
    }

    public Field getField(int x, int y) { return fields[x][y]; }
    public Field getField(Point location) { return getField(location.x, location.y); }
    public void setField(int x, int y, Field field) { fields[x][y] = field; }

    private static Field createField(FieldType fieldType, int level, int meta, Point location, GameLogic logic) {
        Object createdMapSegment = Utility.createMapSegmentByFieldType(fieldType, level, meta, location, logic);
        Field createdField;
        if (fieldType.isEntity) {
            createdField = new Ground(level, location, 0, logic);
            createdField.setContainedEntity((Entity) createdMapSegment);

        } else {
            createdField = (Field) createdMapSegment;
        }
        return createdField;
    }

    public static Field decodeFieldFromColorCode(Color color, Point location, GameLogic logic) {
        int redChannel = color.getRed(), greenChannel = color.getGreen();
        return createField(FieldType.values()[redChannel >> 4], (redChannel & 0b1111), greenChannel, location, logic);
    }

    public static Map createMapFromImage(String imagePath, GameLogic gameLogic) {
        return new Map(Utility.getRGBMatrixFromImage(FilePathConstants.MAPS_PATH + imagePath), gameLogic);
    }

    public static Field[][] getFieldsAroundCoordinate(Field[][] fields, Point coordinate, Dimension distance) {
        final int LEFT_BOUND = 0, RIGHT_BOUND = fields.length - 1, TOP_BOUND = 0, BOTTOM_BOUND = fields[0].length - 1;
        int     left = coordinate.x - distance.width/2, leftRemaining = 0,
                right = coordinate.x + distance.width/2, rightRemaining = 0,
                top = coordinate.y - distance.height/2, topRemaining = 0,
                bottom = coordinate.y + distance.height/2, bottomRemaining = 0;

        if (left < LEFT_BOUND) {
            leftRemaining = LEFT_BOUND - left;
            left = 0;
        }
        if (top < TOP_BOUND) {
            topRemaining = TOP_BOUND - top;
            top = 0;
        }
        if (right > RIGHT_BOUND) {
            rightRemaining = right - RIGHT_BOUND;
            right = RIGHT_BOUND;
        }
        if (bottom > BOTTOM_BOUND) {
            bottomRemaining = bottom - BOTTOM_BOUND;
            bottom = BOTTOM_BOUND;
        }

        if ((leftRemaining == 0 && rightRemaining > 0) || (leftRemaining > 0 && rightRemaining == 0)) {
            if (leftRemaining == 0 && left - rightRemaining >= LEFT_BOUND) left -= rightRemaining;
            else if (right + leftRemaining <= RIGHT_BOUND) right += leftRemaining;
        }

        if ((topRemaining == 0 && bottomRemaining > 0) || (topRemaining > 0 && bottomRemaining == 0)) {
            if (topRemaining == 0 && left - bottomRemaining >= TOP_BOUND) top -= bottomRemaining;
            else if (bottom + topRemaining <= BOTTOM_BOUND) bottom += topRemaining;
        }

        distance.setSize(right + 1 - left, bottom + 1 - top);
        Field[][] fieldsInDistance = new Field[distance.width][distance.height];

        for (int i = 0; i<distance.width; ++i) {
            for (int j = 0; j<distance.height; ++j) {
                fieldsInDistance[i][j] = fields[left + i][top + j];
            }
        }

        return fieldsInDistance;
    }

    public Entity getEntityWithUID(long uid) {
        return getEntities().stream().filter(e -> e.getId() == uid).findFirst().orElse(null);
    }

    public Field[][] getFields() { return fields; }

    public int getMaxLetterNumber() { return this.MAX_LETTER_NUMBER; }

    public List<Character> getLetters() { return this.letters; }

    public static Map getDifferenceMap(Map oldMap, Map newMap) {
        Dimension mapSize = new Dimension(newMap.fields.length, newMap.fields[0].length);
        Field[][] fields = new Field[mapSize.width][mapSize.height];
        for (int i = 0; i<mapSize.width; ++i) {
            for (int j = 0; j<mapSize.height; ++j) {
                if (oldMap.fields.length <= i || oldMap.fields[i].length <= j || !oldMap.fields[i][j].equals(newMap.fields[i][j]))
                    fields[i][j] = newMap.fields[i][j];
            }
        }
        Map difference = new Map(fields);
        difference.players = newMap.players;
        difference.letters = newMap.letters;
        difference.level = newMap.level;
        difference.entities = newMap.entities;
        return difference;
    }

    public static Map calculateNewStateFromDifferenceMap(Map currentMap, Map difference) {
        Dimension mapSize = new Dimension(difference.fields.length, difference.fields[0].length);
        Field[][] fields = new Field[mapSize.width][mapSize.height];
        for (int i = 0; i<mapSize.width; ++i) {
            for (int j = 0; j<mapSize.height; ++j) {
                if (difference.fields[i][j] != null) fields[i][j] = difference.fields[i][j];
                else fields[i][j] = currentMap.fields[i][j];
            }
        }
        Map newMap = new Map(fields, difference.logic);
        newMap.players = difference.players;
        newMap.letters = difference.letters;
        newMap.level = difference.level;
        newMap.entities = difference.entities;
        return newMap;
    }

    public Field[] getFields(Class<? extends Field> fieldClass) {
        List<Field> fieldsWithClass = new LinkedList<>();
        for (Field[] row : fields) {
            for (Field field : row) {
                if (field.getClass().equals(fieldClass)) fieldsWithClass.add(field);
            }
        }
        return fieldsWithClass.toArray(new Field[0]);
    }

    public Point getNextEmptySpawnFieldLocation() {
        return Arrays.stream(getFields(Spawn.class)).filter(s -> s.getContainedEntity() == null).findFirst().get().getLocation();
    }
    public Point getNextEmptyArrivalElevatorPosition() {
        return Arrays.stream(getFields(Elevator.class)).filter(e -> e.getContainedEntity() == null && ((Elevator)e).isDestination()).findFirst().get().getLocation();
    }
}
