package common.map;

import java.awt.*;
import java.util.Arrays;

public enum Direction {
    UP(0, -1, 0),
    DOWN(0, 1, Math.PI),
    LEFT(-1, 0, Math.PI*3./2.),
    RIGHT(1, 0, Math.PI/2.),
    ;
    private final int dx;
    private final int dy;
    private final double rotation;
    private transient Direction oppositeDirection;
    private transient Direction CW;
    private transient Direction CCW;

    static {
        UP.oppositeDirection = DOWN;
        UP.CW = RIGHT;
        UP.CCW = LEFT;
        DOWN.oppositeDirection = UP;
        DOWN.CW = LEFT;
        DOWN.CCW = RIGHT;
        LEFT.oppositeDirection = RIGHT;
        LEFT.CW = DOWN;
        LEFT.CCW = UP;
        RIGHT.oppositeDirection = LEFT;
        RIGHT.CW = UP;
        RIGHT.CCW = DOWN;
    }

    Direction(int dx, int dy, double rotation) {
        this.dx = dx;
        this.dy = dy;
        this.rotation = rotation;
    }

    public int getDx() { return dx; }

    public int getDy() { return dy; }

    public double getRotation() { return rotation; }

    public Direction getCW() { return CW; }

    public Direction getCCW() { return CCW; }

    public Direction getOppositeDirection() { return oppositeDirection; }

    public static Point getDisplacementFromPoints(Point originPoint, Point destinationPoint) {
        return new Point(destinationPoint.x - originPoint.x, destinationPoint.y - originPoint.y);
    }

    public static Direction getDirectionFromDisplacement(int x, int y) {
        return Arrays.stream(Direction.values()).filter(d -> d.dx == x && d.dy == y).findFirst().orElse(null);
    }
}
