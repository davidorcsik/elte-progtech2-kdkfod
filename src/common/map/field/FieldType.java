package common.map.field;

import common.map.entity.furniture.Furniture;
import common.map.entity.panel.Panel;
import common.map.entity.pickup.Pickup;
import common.map.entity.robot.Robot;

import java.util.Arrays;

public enum FieldType {
    WALL(Wall.class,            false),
    ROBOT(Robot.class,          true),
    SPAWN(Spawn.class,          false),
    FURNITURE(Furniture.class,  true),
    PANEL(Panel.class,          true),
    ELEVATOR(Elevator.class,    false),
    PICKUP(Pickup.class,        true),
    GROUND(Ground.class,        false),
    ;
    public final Class associatedClassName;
    public final boolean isEntity;


    FieldType(Class classObj, boolean isEntity) { this.associatedClassName = classObj; this.isEntity = isEntity; }

    public static FieldType getFieldTypeFromClassName(Class associatedClassName) {
        return Arrays.stream(FieldType.values())
                .filter(fieldType -> fieldType.associatedClassName.equals(associatedClassName)).findAny().orElse(null);
    }
}
