package common.map.field;

import server.logic.GameLogic;

import java.awt.*;

public class Wall extends Field {
    private FieldType fieldType;

    public Wall(Integer level, Point location, Integer meta, GameLogic logic) {
        super(level, location, logic, Wall.class);
        fieldType = FieldType.WALL;
    }

    public Wall(Wall other) { super(other); }

    public void setFieldType(FieldType fieldType) { this.fieldType = fieldType; }

    @Override
    public FieldType getFieldType() { return fieldType; }

    @Override
    public int getMeta() { return 0; }

    @Override
    protected void parseMeta(Integer meta) {}

    @Override
    public String toString() { return getFieldType() + " " + super.toString(); }

    @Override
    public boolean isStepable() { return false; }
}
