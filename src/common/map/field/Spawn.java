package common.map.field;

import server.logic.GameLogic;

import java.awt.*;

public class Spawn extends Field {
    public Spawn(Integer associatedLevel, Point location, Integer meta, GameLogic logic) {
        super(associatedLevel, location, logic, Spawn.class);
    }

    public Spawn(Spawn other) { super(other); }

    @Override
    public FieldType getFieldType() { return FieldType.SPAWN; }

    @Override
    public int getMeta() { return 0; }

    @Override
    protected void parseMeta(Integer meta) {}

    @Override
    public String toString() { return getFieldType() + " " + super.toString(); }
}
