package common.map.field;

import server.logic.GameLogic;

import java.awt.*;
import java.util.Objects;

public class Elevator extends Field {
    private int destinationLevel;
    private boolean isDestination;

    public Elevator(Integer associatedLevel, Point location, Integer meta, GameLogic logic){
        super(associatedLevel, location, logic, Elevator.class);
        parseMeta(meta);
    }

    public Elevator(Elevator other) {
        super(other);
        this.destinationLevel = other.destinationLevel;
        this.isDestination = other.isDestination;
    }

    @Override
    protected void parseMeta(Integer metaBoxed) {
        int meta = metaBoxed; //muszály az unboxing mert nem az Integer osztály bitjeit akarjuk tologatni
        destinationLevel = meta >> 4;
        isDestination = ((meta >> 3) & 0b1) == 1;
    }

    @Override
    public FieldType getFieldType() { return FieldType.ELEVATOR; }

    @Override
    public int getMeta() { return (destinationLevel << 4) | (isDestination ? 1 : 0) << 3; }

    @Override
    public String toString() { return getFieldType() + " " + super.toString(); }

    public int getDestinationLevel() { return destinationLevel; }

    public boolean isDestination() { return isDestination; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Elevator elevator = (Elevator) o;
        return destinationLevel == elevator.destinationLevel &&
                isDestination == elevator.isDestination;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), destinationLevel, isDestination);
    }
}
