package common.map.field;

import common.map.entity.Entity;
import common.map.entity.pickup.Pickup;
import common.utility.Utility;
import server.logic.GameLogic;

import java.awt.*;
import java.io.Serializable;
import java.util.Objects;

public abstract class Field implements Serializable {
    private Point location;
    private int associatedLevel;
    private final transient GameLogic logic;
    private Entity containedEntity;

    public Field(int associatedLevel, Point location, GameLogic logic, Class<? extends Field> subclass) {
        this.associatedLevel = associatedLevel;
        this.location = location;
        this.logic = logic;
    }

    public Field(Field other) {
        this.location = new Point(other.location);
        this.associatedLevel = other.associatedLevel;
        this.logic = other.logic; //Nem másolható!
        this.containedEntity = (Entity) Utility.copyObject(other.containedEntity);
    }

    @Override
    public String toString() { return "level: " + associatedLevel + " x:" + this.location.x + " y:" + this.location.y; }

    public abstract FieldType getFieldType();

    public abstract int getMeta();

    protected abstract void parseMeta(Integer meta);

    public Color getColorRepresentation() {
        int redChannel = FieldType.getFieldTypeFromClassName(this.getClass()).ordinal() << 4 | associatedLevel,
            greenChannel = getMeta(),
            blueChannel = 0;
        return new Color(redChannel, greenChannel, blueChannel);
    }

    public Point getLocation() { return location; }

    public void setLocation(Point location) { this.location = location; }

    public int getAssociatedLevel() { return associatedLevel; }

    public void setContainedEntity(Entity entity) { containedEntity = entity; }

    public Entity getContainedEntity() { return containedEntity; }

    public boolean isStepable() {
        return (containedEntity==null || containedEntity.getClass().equals(Pickup.class)) && !getFieldType().equals(FieldType.WALL);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Field field = (Field) o;
        return associatedLevel == field.associatedLevel &&
                location.equals(field.location) &&
                Objects.equals(containedEntity, field.containedEntity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location, associatedLevel, containedEntity);
    }
}
