package common.map.field;

import server.logic.GameLogic;

import java.awt.*;

public class Ground extends Field {
    public Ground(Integer associatedLevel, Point location, Integer meta, GameLogic logic) { super(associatedLevel, location, logic, Ground.class); }

    public Ground(Ground other) { super(other); }

    @Override
    public FieldType getFieldType() {
        return FieldType.GROUND;
    }

    @Override
    public int getMeta() {
        return 0;
    }

    @Override
    protected void parseMeta(Integer meta) {

    }
}
