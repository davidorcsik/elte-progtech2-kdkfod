package common.map.entity;

import common.map.Direction;
import server.logic.GameLogic;

import java.awt.*;

public interface Actor {

    void setId(long id);

    Point getSpawnLocation();

    Direction getFacingDirection();

    void setFacingDirection(Direction direction);

    void setLocation(Point location);

    void setSpawnLocation(Point spawnLocation);

    int getMeta();

    GameLogic getGameLogic();

    Point getLocation();

    long getId();

    boolean isAlive();

    void damage(int damage);

    void kill();

    int getAmmoAmount();
}
