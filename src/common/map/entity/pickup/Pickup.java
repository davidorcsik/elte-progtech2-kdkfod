package common.map.entity.pickup;

import common.map.entity.Entity;
import common.map.exception.FieldParseException;
import common.map.field.FieldType;
import server.logic.GameLogic;

import java.awt.*;
import java.util.Objects;

public class Pickup extends Entity {
    private PickupType type;

    public Pickup(Integer associatedLevel, Point location, Integer meta, GameLogic logic) {
        super(location, associatedLevel, logic);
        parseMeta(meta);
    }

    public Pickup(Pickup other) {
        super(other);
        this.type = other.type; //Immutable
    }

    protected void parseMeta(Integer metaBoxed) {
        int meta = metaBoxed; //muszály az unboxing mert nem az Integer osztály bitjeit akarjuk tologatni
        try { type = PickupType.values()[(meta >> 6) & 0b1]; }
        catch (IndexOutOfBoundsException e) { throw new FieldParseException("Cannot parse meta!"); }
    }

    public FieldType getFieldType() { return FieldType.PICKUP; }

    @Override
    public int getMeta() { return type.ordinal() << 7; }

    @Override
    public String toString() { return getFieldType() + " " + super.toString(); }

    public PickupType getType() { return type; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pickup pickup = (Pickup) o;
        return type == pickup.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), type);
    }
}
