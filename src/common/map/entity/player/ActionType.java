package common.map.entity.player;

public enum ActionType {
    INTERACT, ATTACK, DEACTIVATION_CODE, MOVE
}
