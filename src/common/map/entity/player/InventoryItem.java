package common.map.entity.player;

import java.io.Serializable;
import java.util.Objects;

public class InventoryItem implements Serializable {
    private final char character;
    private final int level;
    private final long furnitureUID;

    public InventoryItem(char character, int level, long furnitureUID) {
        this.character = character;
        this.level = level;
        this.furnitureUID = furnitureUID;
    }

    public char getCharacter() { return character; }

    public long getFurnitureUID() { return furnitureUID; }

    public int getLevel() { return level; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InventoryItem that = (InventoryItem) o;
        return character == that.character &&
                level == that.level &&
                furnitureUID == that.furnitureUID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(character, level, furnitureUID);
    }
}
