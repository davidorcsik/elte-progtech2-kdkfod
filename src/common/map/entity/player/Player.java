package common.map.entity.player;

import common.map.Direction;
import common.map.entity.Actor;
import common.map.entity.Entity;
import common.map.entity.pickup.Pickup;
import common.map.entity.pickup.PickupType;
import server.logic.GameLogic;

import java.awt.*;
import java.util.*;
import java.util.List;

public class Player extends Entity implements Actor {
    public static final int MAX_HEALTH = 1000;         //A maximális-, kezdő- és újraéledési életpont
    public static final int TOP_HEALTH_MULTIPLIER = 2;
    public static final int START_AMMO = 15;          //A töltényeid száma kezdéskor/újraéledéskor
    public static final int MAX_AMMO = 50;
    private final int HP_PICKUP_VALUE = 200;     //Mennyi életpontot kapsz, ha felveszel egy "hp pickup"-ot
    private final int AMMO_PICKUP_VALUE = 15;   //Mennyi töltényt kapsz, ha felveszel egy "ammo pickup"-ot

    private int healthPoint = MAX_HEALTH;                               //A játékos életpontjai
    private int ammoAmount = START_AMMO;                                //A játékos töltényeinek száma
    private List<InventoryItem> inventory = new ArrayList<>();                 //A játékos inventory-ja
    private boolean isAlive = true;                                     //Életben van-e a játékos
    private boolean canMove = true;                                     //Tud-e mozogni a játékos

    public Player(Integer associatedLevel, Point location, Integer meta, GameLogic logic) {
        super(location, associatedLevel, logic);
        setFacingDirection(Direction.UP);
    }

    public Player(Player other) {
        super(other);
        this.healthPoint = other.healthPoint;
        this.ammoAmount = other.ammoAmount;
        this.inventory = new ArrayList<>(other.inventory);
        this.isAlive = other.isAlive;
        this.canMove = other.canMove;
    }

    public void kill() {
        isAlive = false;
        canMove = false;
    }

    public void damage(int damage) {
        healthPoint-=damage;
        if(healthPoint<=0) {
            healthPoint=0;
            kill();
        }
    }

    public void shoot() { if(ammoAmount>0) ammoAmount--; }

    public void pickUp(Pickup pickup) {
        if(pickup.getType().equals(PickupType.HP)) setHealthPoint(healthPoint+ HP_PICKUP_VALUE);
        else setAmmoAmount(ammoAmount+ AMMO_PICKUP_VALUE);
    }

    public void respawn(Point spawnLocation) {
        setLocation(spawnLocation);
        healthPoint = MAX_HEALTH;
        ammoAmount = START_AMMO;
        isAlive = true;
        canMove = true;
    }

    public void setHealthPoint(int healthPoint) {
        if(healthPoint>=MAX_HEALTH*TOP_HEALTH_MULTIPLIER) this.healthPoint = MAX_HEALTH*TOP_HEALTH_MULTIPLIER;
        else this.healthPoint = healthPoint;
    }

    public void appendToInventory(InventoryItem item) {
        if(item==null) throw new IllegalArgumentException("Cannot append null");
        inventory.add(item);
    }

    @Override
    public String toString() {
        return "[Player: " + getId()
                + "], [Location: " + getLocation()
                + "], [Inventory: " + getInventory()
                + "], [Spawn point: " + getSpawnLocation()
                + "], [Facing: " + getFacingDirection()
                + "], [HP: " + healthPoint
                + "], [Ammo: " + ammoAmount
                + "], [Alive?: " + isAlive
                + "], [Can move?: " + canMove + "]";
    }

    public void setSpawnLocation(Point spawnLocation) { super.setSpawnLocation(spawnLocation); }

    public int getHealthPoint() { return healthPoint; }

    public List<InventoryItem> getInventory() { return inventory; }

    public boolean isAlive() { return isAlive; }

    public boolean canMove() { return canMove; }

    public void setMove(boolean canMove) { this.canMove = canMove; }

    public void setInventory(List<InventoryItem> inventory) { this.inventory = new LinkedList<>(inventory); }

    public void setAmmoAmount(int ammoAmount) { this.ammoAmount = ammoAmount; }

    public int getAmmoAmount() { return this.ammoAmount; }

    @Override
    public int getMeta() { return 0; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return healthPoint == player.healthPoint &&
                ammoAmount == player.ammoAmount &&
                isAlive == player.isAlive &&
                canMove == player.canMove &&
                inventory.equals(player.inventory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), healthPoint, ammoAmount, inventory, isAlive, canMove);
    }
}
