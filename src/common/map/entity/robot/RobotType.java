package common.map.entity.robot;

import java.util.Arrays;

public enum RobotType {
    AGGRESSIVE(RobotBehaviourAggressive.class),
    TACTICAL(RobotBehaviourTactical.class),
    ;
    public final Class associatedClassName;

    RobotType(Class classObj) { this.associatedClassName = classObj; }

    public static RobotType getRobotTypeFromClassName(Class associatedClassName) {
        return Arrays.stream(RobotType.values())
                .filter(fieldType -> fieldType.associatedClassName.equals(associatedClassName)).findAny().orElse(null);
    }
}
