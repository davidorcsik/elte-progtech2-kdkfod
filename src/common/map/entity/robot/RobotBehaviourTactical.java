package common.map.entity.robot;

import common.map.Direction;

import java.util.LinkedList;

public class RobotBehaviourTactical extends RobotBehaviour {
    private static int ALARM_DISTANCE = 5;

    public RobotBehaviourTactical(Robot associatedRobot) {
        super(associatedRobot, ALARM_DISTANCE);
    }

    public RobotBehaviourTactical(RobotBehaviourTactical other) { super(other); }


}
