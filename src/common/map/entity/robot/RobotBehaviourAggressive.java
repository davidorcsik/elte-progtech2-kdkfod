package common.map.entity.robot;

import common.map.Direction;
import common.map.Map;
import common.map.field.Field;
import common.map.field.FieldType;

public class RobotBehaviourAggressive extends RobotBehaviour {
    private static int ALARM_DISTANCE = 10;

    public RobotBehaviourAggressive(Robot associatedRobot) {
        super(associatedRobot,ALARM_DISTANCE);
    }

    public RobotBehaviourAggressive(RobotBehaviourAggressive other) { super(other); }

    public Direction playerInLine() {
        Map map = getAssociatedRobot().getGameLogic().getMap();
        Field currentField = map.getField(getAssociatedRobot().getLocation());
        while(currentField.getFieldType()!=FieldType.WALL) {
            Field nextField = map.getField(currentField.getLocation().x, currentField.getLocation().y-1);
            if(nextField.getFieldType().equals(FieldType.GROUND)) currentField = nextField;
            else return null;
        }
        return null;
    }
}