package common.map.entity.robot;

import common.map.Direction;
import common.map.Map;
import common.map.entity.Entity;
import common.map.entity.pickup.Pickup;
import common.map.entity.player.ActionType;
import common.map.entity.player.Player;
import common.map.field.Field;
import common.map.field.FieldType;
import common.utility.Message;
import common.utility.MessageType;
import common.utility.Utility;

import java.awt.*;
import java.util.*;
import java.util.List;

import static common.map.Direction.*;

public abstract class RobotBehaviour {
    private final Robot associatedRobot;
    private int alarmDistance;
    private Field[][] patrolFields;
    private int tickCounter = 0;

    protected RobotBehaviour(Robot associatedRobot, int alarmDistance) {
        this.associatedRobot = associatedRobot;
        this.alarmDistance = alarmDistance;
    }

    public RobotBehaviour(RobotBehaviour other) {
        this.associatedRobot = other.associatedRobot; //Circular reference
        this.alarmDistance = other.alarmDistance;
    }

    public void setAlarmDistance(int distance) { this.alarmDistance = distance; }

    public Robot getAssociatedRobot() { return associatedRobot; }

    public void createPatrolFields() {
        patrolFields = Map.getFieldsAroundCoordinate(associatedRobot.getGameLogic().getMap().getFields(), associatedRobot.getLocation(), new Dimension(10,10));
    }

    public Field[][] getPatrolFields() {
        return patrolFields;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RobotBehaviour that = (RobotBehaviour) o;
        return alarmDistance == that.alarmDistance;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), associatedRobot, alarmDistance);
    }

    private boolean isFieldContainEnemy(Field field) {
        return field != null && field.getContainedEntity() instanceof Player;
    }

    public Entity[] getEnemiesInFOV() {
        Map map = associatedRobot.getGameLogic().getMap();
        Point currentPosition = associatedRobot.getLocation();
        Direction facingDirection = associatedRobot.getFacingDirection();
        LinkedList<Entity> enemies = new LinkedList<>();

        Point currentAxeCoordinate = Utility.getPointInDirection(currentPosition, facingDirection);
        for (int i = 0; i < alarmDistance; ++i, currentAxeCoordinate = Utility.getPointInDirection(currentAxeCoordinate, facingDirection)) {
            Point currentFieldPosition = Utility.getPointInDirection(currentAxeCoordinate, facingDirection.getCCW());
            for (int j = 0; j <= i; ++j, currentFieldPosition = Utility.getPointInDirection(currentFieldPosition, facingDirection.getCCW())) {
                Field currentField = map.getField(currentFieldPosition);
                if (isFieldContainEnemy(currentField)) enemies.add(currentField.getContainedEntity());
            }

            if (isFieldContainEnemy(map.getField(currentAxeCoordinate))) enemies.add(map.getField(currentAxeCoordinate).getContainedEntity());

            currentFieldPosition = Utility.getPointInDirection(currentAxeCoordinate, facingDirection.getCW());
            for (int j = 0; j <= i; ++j, currentFieldPosition = Utility.getPointInDirection(currentFieldPosition, facingDirection.getCW())) {
                Field currentField = map.getField(currentFieldPosition);
                if (isFieldContainEnemy(currentField)) enemies.add(currentField.getContainedEntity());
            }
        }

        return enemies.toArray(new Entity[0]);
    }

    public void patrolMode() {
        if(tickCounter==3) {
            tickCounter=0;
            Point currentPosition = associatedRobot.getLocation();
            Direction facingDirection = associatedRobot.getFacingDirection();
            Point nextPosition = Utility.getPointInDirection(currentPosition, facingDirection);
            boolean switcher = false;

            try {
                for (Field[] patrolField : patrolFields) {
                    for (Field field : patrolField) {
                        if (field.getLocation().equals(nextPosition)) switcher = true;
                    }
                }
            } catch (NullPointerException e) {}


            if(switcher && associatedRobot.getGameLogic().getMap().getField(nextPosition).isStepable()) {
                List<Integer> chance = new ArrayList<>(Arrays.asList(0,1,2));
                Collections.shuffle(chance);
                if(chance.get(0)==2) {
                    associatedRobot.incomingMessages.add(Message.createMessageFromIntent(nextPosition, ActionType.MOVE, associatedRobot.getId()));
                } else {
                    associatedRobot.incomingMessages.add(Message.createMessageFromIntent(nextPosition, ActionType.ATTACK, associatedRobot.getId()));
                }
            } else {
                Direction[] d = {facingDirection.getOppositeDirection(), facingDirection.getCW(), facingDirection.getCCW()};
                Collections.shuffle(Arrays.asList(d));
                associatedRobot.setFacingDirection(d[0]);
            }
        } else {
            tickCounter++;
        }
    }
}
