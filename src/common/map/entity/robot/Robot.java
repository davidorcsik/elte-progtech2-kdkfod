package common.map.entity.robot;

import common.map.Direction;
import common.map.entity.Actor;
import common.map.entity.Entity;
import common.map.exception.FieldParseException;
import common.map.field.FieldType;
import common.utility.Message;
import common.utility.Utility;
import server.logic.GameLogic;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

public class Robot extends Entity implements Actor {
    private final int MAX_HEALTH = 600;

    private transient RobotBehaviour behaviour;
    private RobotType type;
    private int healthPoint;
    private int ammoAmount;
    private boolean isAlive;
    private int alarmDistance;
    final Queue<Message<?>> incomingMessages = new LinkedList<>();

    public Robot(Integer associatedLevel, Point location, Integer meta, GameLogic logic) {
        super(location, associatedLevel, logic);
        healthPoint = MAX_HEALTH;
        isAlive = true;
        parseMeta(meta);
        ammoAmount=1;
    }

    public Robot(Robot other) {
        super(other);
        this.behaviour = (RobotBehaviour) Utility.copyObject(other.behaviour);
        this.type = other.type; //Immutable
        this.healthPoint = other.healthPoint;
        this.isAlive = other.isAlive;
        this.alarmDistance = other.alarmDistance;
        ammoAmount=1;
    }

    public void damage(int damage) {
        healthPoint-=damage;
        if(healthPoint<=0) kill();
    }

    public void kill() {
        isAlive = false;
        getGameLogic().getMap().removeEntity(this);
    }

    protected void parseMeta(Integer metaBoxed) {
        int meta = metaBoxed; //muszály az unboxing mert nem az Integer osztály bitjeit akarjuk tologatni
        try {
            type = RobotType.values()[meta >> 6];
            behaviour = (RobotBehaviour) type.associatedClassName.getConstructor(Robot.class)
                    .newInstance(this);
            setFacingDirection(Direction.values()[(meta >> 4) & 0b11]);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | IndexOutOfBoundsException e) {
            throw new FieldParseException("Cannot parse meta!");
        }
    }

    private void setAlarmDistance() {
        if(type.equals(RobotType.TACTICAL)) alarmDistance = 1;
    }

    @Override
    public String toString() {
        return super.toString()
                + "\nRobot's HP: " + healthPoint
                + "\nRobot's behaviour: " + behaviour
                + "\nIs Alive?: " + isAlive
                + "\nType: " + getFieldType()
                + "\nAlarm distance: " + alarmDistance;
    }

    public FieldType getFieldType() {
        return FieldType.ROBOT;
    }

    @Override
    public int getMeta() { return (RobotType.getRobotTypeFromClassName(behaviour.getClass()).ordinal() << 6) | (getFacingDirection().ordinal() << 4); }

    @Override
    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean bool) { this.isAlive = bool; }

    public RobotBehaviour getBehaviour() { return behaviour; }

    public void setBehaviour(RobotBehaviour r) { this.behaviour = r; }

    public RobotType getType() { return type; }

    public int getAmmoAmount() { return this.ammoAmount; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Robot robot = (Robot) o;
        return healthPoint == robot.healthPoint &&
                isAlive == robot.isAlive &&
                alarmDistance == robot.alarmDistance &&
                behaviour.equals(robot.behaviour) &&
                type == robot.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), behaviour, type, healthPoint, isAlive, alarmDistance);
    }

    public LinkedList<Message> getStateChanges() {
        LinkedList<Message> temp = new LinkedList<>();
        Message currentMessage;
        while ((currentMessage = incomingMessages.poll()) != null) temp.add(currentMessage);
        return temp;
    }
}
