package common.map.entity;

import common.utility.Message;
import common.utility.Utility;
import common.map.Direction;
import server.logic.GameLogic;

import java.awt.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

public abstract class Entity implements Serializable {
    private final transient GameLogic gameLogic;
    private Point spawnLocation;
    private long id;
    private Direction facingDirection;
    private Point location;

    public Entity(Point spawnLocation, int associatedLevel, GameLogic logic) {
        this.spawnLocation = new Point(spawnLocation);
        this.location = new Point(spawnLocation);
        this.gameLogic = logic;
        this.facingDirection = Direction.DOWN;
    }

    public Entity(Entity other) {
        this.gameLogic = other.gameLogic; //Nem másolható!
        this.spawnLocation = new Point(other.spawnLocation);
        this.id = other.id;
        this.facingDirection = other.facingDirection; //Immutable
        this.location = new Point(other.location);
    }

    public void setId(long id) {
        this.id = id;
    }

    public Point getSpawnLocation() { return spawnLocation; }

    public void setSpawnLocation(Point spawnLocation) { this.spawnLocation.setLocation(spawnLocation); }

    public Direction getFacingDirection() { return facingDirection; }

    public void setFacingDirection(Direction direction) { facingDirection = direction; }

    public void setLocation(Point location) { this.location.setLocation(location); }

    public abstract int getMeta();

    public GameLogic getGameLogic() { return gameLogic; }

    public Point getLocation() { return location; }

    @Override
    public String toString() {
        return "Spawn Location (X): " + spawnLocation.getX()
                + "\nSpawn Location (Y): " + spawnLocation.getY()
                + "\nCurrent facing direction: " + facingDirection.toString();
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity entity = (Entity) o;
        return id == entity.id &&
                spawnLocation.equals(entity.spawnLocation) &&
                facingDirection == entity.facingDirection &&
                location.equals(entity.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(spawnLocation, id, facingDirection, location);
    }

}
