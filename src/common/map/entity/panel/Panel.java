package common.map.entity.panel;

import common.map.entity.Entity;
import common.map.entity.player.Player;
import server.logic.GameLogic;
import common.map.Direction;
import common.map.exception.FieldParseException;
import common.map.field.FieldType;

import java.awt.*;

public class Panel extends Entity {

    public Panel(Integer associatedLevel, Point location, Integer meta, GameLogic logic) {
        super(location, associatedLevel, logic);
        parseMeta(meta);
    }

    public Panel(Panel other) { super(other); }

    protected void parseMeta(Integer metaBoxed) {
        int meta = metaBoxed; //muszály az unboxing mert nem az Integer osztály bitjeit akarjuk tologatni
        try { setFacingDirection(Direction.values()[(meta >> 6) & 0b11]); }
        catch (IndexOutOfBoundsException e) { throw new FieldParseException("Cannot parse meta!"); }
    }

    public FieldType getFieldType() { return FieldType.PANEL; }

    public int getMeta() { return getFacingDirection().ordinal() << 6; }

    @Override
    public String toString() { return super.toString() + "\nType: " + getFieldType(); }
}
