package common.map.entity;

import common.map.Direction;
import common.map.entity.player.Player;
import common.map.entity.robot.Robot;
import common.utility.Utility;
import server.logic.GameLogic;
import common.map.field.FieldType;

import java.awt.*;
import java.util.Objects;

public class Projectile extends Entity {
    private final int DAMAGE_VALUE = 200;
    private final Actor hostEntity;
    private boolean isAlive = true;

    public Projectile(Point spawnLocation, GameLogic logic, Direction direction, Actor hostEntity) {
        super(spawnLocation, 0, logic);
        super.setFacingDirection(direction);
        this.hostEntity = hostEntity;
    }

    public Projectile(Projectile other) {
        super(other);
        this.hostEntity = other.hostEntity;
    }

    public void move() { super.setLocation(Utility.getPointInDirection(super.getLocation(), super.getFacingDirection())); }

    public Point getNextField() { return Utility.getPointInDirection(getLocation(), getFacingDirection()); }

    public Class getTargetEntityType() {
        if(this.hostEntity.getClass().equals(Player.class)) return Robot.class;
        else if(this.hostEntity.getClass().equals(Robot.class)) return Player.class;
        else return null;
    }

    public Class getHostEntityType() { return this.hostEntity.getClass(); }

    public int getDamageValue() { return this.DAMAGE_VALUE; }

    public FieldType getFieldType() {
        return null;
    }

    public boolean isAlive() { return this.isAlive; }

    public void kill() { this.isAlive = false; }

    @Override
    public int getMeta() { return 0; }

    protected void parseMeta(Integer meta) {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Projectile that = (Projectile) o;
        return DAMAGE_VALUE == that.DAMAGE_VALUE &&
                hostEntity.equals(that.hostEntity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), DAMAGE_VALUE, hostEntity);
    }
}
