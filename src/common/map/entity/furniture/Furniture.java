package common.map.entity.furniture;

import common.map.Direction;
import common.map.entity.Entity;
import common.map.exception.FieldParseException;
import common.map.field.FieldType;
import server.logic.GameLogic;

import java.awt.*;
import java.util.Objects;

public class Furniture extends Entity {
    private FurnitureType type;
    private Dimension size;
    private Character containedLetter;

    public Furniture(Integer associatedLevel, Point location, Integer meta, GameLogic logic) {
        super(location, associatedLevel, logic);
        parseMeta(meta);
    }

    public Furniture(Furniture other) {
        super(other);
        this.type = other.type; //Immutable
        this.size = new Dimension(other.size);
        this.containedLetter = other.containedLetter;
    }

    protected void parseMeta(Integer metaBoxed) {
        int meta = metaBoxed; //muszály az unboxing mert nem az Integer osztály bitjeit akarjuk tologatni
        type = FurnitureType.values()[meta >> 6];
        try { setFacingDirection(Direction.values()[(meta >> 4) & 0b11]); }
        catch (IndexOutOfBoundsException e) { throw new FieldParseException("Cannot parse meta!"); }
        size = new Dimension(((meta >> 2) & 0b11) + 1, (meta & 0b11) + 1);
    }

    public FurnitureType getType() { return type; }

    public int getMeta() { return (type.ordinal() << 6) | (getFacingDirection().ordinal() << 4) | ((size.width - 1) << 2) | (size.height - 1); }

    @Override
    public String toString() {
        return super.toString()
                + "\nType: " + getType()
                + "\nFurniture type: " + getFurnitureType()
                + "\nDimension (height): " + size.height
                + "\nDimension (width):" + size.width;
    }

    public boolean hasLetter() { return containedLetter != null; }

    public FieldType getFieldType() {
        return FieldType.FURNITURE;
    }

    public FurnitureType getFurnitureType() { return type; }

    public void setType(FurnitureType type) { this.type = type;}

    public Dimension getSize() { return size; }

    public Character getContainedLetter() { return this.containedLetter; }

    public void setContainedLetter(Character character) { this.containedLetter = character; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Furniture furniture = (Furniture) o;
        return containedLetter == furniture.containedLetter &&
                type == furniture.type &&
                size.equals(furniture.size);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), type, size, containedLetter);
    }
}
