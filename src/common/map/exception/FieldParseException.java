package common.map.exception;

public class FieldParseException extends RuntimeException {
    public FieldParseException() { }
    public FieldParseException(String message) { super(message); }
    public FieldParseException(String message, Throwable cause) { super(message, cause); }
}
