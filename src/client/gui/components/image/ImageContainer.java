package client.gui.components.image;

import client.gui.Component;
import client.gui.ComponentProportion;
import client.gui.IComponent;
import client.logic.GameClient;
import client.utility.Utility;

import java.awt.*;
import java.awt.image.ImageObserver;

public class ImageContainer extends Component {
    private final Image containedImage;
    public ImageContainer(IComponent offsetFrom, Utility.OffsetDirection offsetDirection, ComponentProportion proportion, ComponentProportion gapProportion, IComponent parentComponent, GameClient gameClient, Image containedImage) {
        super(offsetFrom, offsetDirection, proportion, gapProportion, parentComponent, gameClient);
        this.containedImage = containedImage;
    }

    @Override
    public void draw(Graphics2D g) {
        Point offset = getOffset();
        Dimension size = getSize();
        ImageObserver observer = null;
        try { observer = (ImageObserver) getContainer(); }
        catch (ClassCastException ignored) {}
        g.drawImage(containedImage, offset.x, offset.y, size.width, size.height, observer);
    }
}
