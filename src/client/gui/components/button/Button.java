package client.gui.components.button;

import client.gui.Component;
import client.gui.ComponentProportion;
import client.gui.IComponent;
import client.logic.GameClient;
import client.utility.DrawHelper;
import client.utility.Utility;

import java.awt.*;

public abstract class Button extends Component {
    private String text;
    private Color color;
    private final double textProportion = 0.1;

    public Button(IComponent offsetFrom, Utility.OffsetDirection offsetDirection, ComponentProportion proportion, ComponentProportion gapProportion, IComponent parentComponent, GameClient gameClient, ButtonEventListener eventListener, Color color) {
        super(offsetFrom, offsetDirection, proportion, gapProportion, parentComponent, gameClient);
        this.color = color;
        eventListener.addButton(this);
    }

    public String getText() { return text; }

    public void setText(String text) { this.text = text; }

    public Color getColor() { return color; }

    public void setColor(Color color) { this.color = color; }

    @Override
    public void draw(Graphics2D g) {
        DrawHelper.drawFilledRect(g, getOffset(), getSize(), Color.gray);
        DrawHelper.drawCenteredString(g, text, new Rectangle(getOffset(), getSize()), new Font("Arial", Font.BOLD, 12), Color.white);
    }

    public abstract void click();
}
