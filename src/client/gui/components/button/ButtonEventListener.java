package client.gui.components.button;

import client.utility.Utility;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class ButtonEventListener {
    private final List<Button> buttons = new ArrayList<>();

    public void addButton(Button button) {
        if (button == null) throw new IllegalArgumentException("Can't connect event to null!");
        buttons.add(button);
    }

    public void click(Point cursorPosition) {
        buttons.stream().filter(b ->
            Utility.isPointInRect(new Rectangle(b.getOffset(), b.getSize()), cursorPosition))
            .findFirst().ifPresent(Button::click);
    }
}
