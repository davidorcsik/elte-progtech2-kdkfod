package client.gui.front_page;

import client.logic.GameClient;

import javax.swing.*;
import java.awt.*;

public class FrontPage extends JFrame {
    private final GameClient gameClient;

    public FrontPage(GameClient gameClient) {
        this.gameClient = gameClient;
        setDefaults();
        addControls();
    }

    private void addControls() {
        add(new ContainerPanel(gameClient));
    }

    private void setDefaults() {
        setLayout(new BorderLayout());
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setSize(614, 400); //FIXME: valahogy a méretet függetleníteni az oprendszertől
        setVisible(true);
    }
}
