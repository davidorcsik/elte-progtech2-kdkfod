package client.gui.front_page;

import client.gui.ComponentProportion;
import client.gui.IComponent;
import client.gui.components.button.ButtonEventListener;
import client.gui.components.image.ImageContainer;
import client.gui.front_page.components.CloseButton;
import client.logic.GameClient;
import client.utility.ImageLoader;
import client.utility.Utility;
import common.utility.FilePathConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ConnectPanel extends JPanel implements IComponent {
    private final JFrame parent;
    private final ButtonEventListener closeButtonClickEventListener = new ButtonEventListener();
    private final CloseButton closeButton;
    private final ImageContainer loadingImageContainer;
    private final Image loadingImage = ImageLoader.getResourceImageIconWithSize(FilePathConstants.IMAGES_PATH + "loadingImage.gif", 267, 267);
    private final ComponentProportion centeringOffsetProportion = new ComponentProportion(5./30., 3./30.);
    private final ComponentProportion loadingPictureProportion = new ComponentProportion(20./30., 20./30.);
    private final ComponentProportion gapProportion = new ComponentProportion(ComponentProportion.BASE_PROPORTION, 2./30.);
    private final ComponentProportion closeButtonProportion = new ComponentProportion(20./30., 3./30.);


    ConnectPanel(JFrame parent, GameClient gameClient) {
        this.parent = parent;
        addMouseListener(mouseListener);
        setLayout(new BorderLayout());
        setSize(new Dimension(400,400));
        setBackground(Color.white);

        loadingImageContainer = new ImageContainer(this, Utility.OffsetDirection.NONE, loadingPictureProportion, null, this, null, loadingImage);
        closeButton = new CloseButton(loadingImageContainer, Utility.OffsetDirection.VERTICAL, closeButtonProportion, gapProportion, this, gameClient, closeButtonClickEventListener, Color.gray);
    }

    @Override
    public Point getOffset() {
        return Utility.convertDimensionToPoint(Utility.scaleWithProportion(getSize(), centeringOffsetProportion));
    }

    public MouseListener mouseListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            super.mouseClicked(e);
            closeButtonClickEventListener.click(e.getPoint());
        }
    };

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        loadingImageContainer.draw((Graphics2D) g);
        closeButton.draw((Graphics2D) g);
    }

    public void dispose() {
        parent.dispose();
    }
}
