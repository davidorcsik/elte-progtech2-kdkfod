package client.gui.front_page.components;

import client.gui.ComponentProportion;
import client.gui.IComponent;
import client.gui.components.button.Button;
import client.gui.components.button.ButtonEventListener;
import client.logic.GameClient;
import client.utility.Utility;

import java.awt.*;

public class ExitButton extends Button {
    public ExitButton(IComponent offsetFrom, Utility.OffsetDirection offsetDirection, ComponentProportion proportion, ComponentProportion gapProportion, IComponent parentComponent, GameClient gameClient, ButtonEventListener eventListener, Color color) {
        super(offsetFrom, offsetDirection, proportion, gapProportion, parentComponent, gameClient, eventListener, color);
        setText("Exit");
    }

    @Override
    public void click() {
        getGameClient().exit();
    }
}
