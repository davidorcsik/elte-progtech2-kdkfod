package client.gui.front_page.components;

import client.gui.ComponentProportion;
import client.gui.IComponent;
import client.gui.components.button.Button;
import client.gui.components.button.ButtonEventListener;
import client.gui.front_page.ConnectWindow;
import client.logic.GameClient;
import client.utility.Utility;

import javax.swing.*;
import java.awt.*;

public class ConnectButton extends Button {
    public ConnectButton(IComponent offsetFrom, Utility.OffsetDirection offsetDirection, ComponentProportion proportion, ComponentProportion gapProportion, IComponent parentComponent, GameClient gameClient, ButtonEventListener eventListener, Color color) {
        super(offsetFrom, offsetDirection, proportion, gapProportion, parentComponent, gameClient, eventListener, color);
        setText("Connect to game");
    }

    @Override
    public void click() {
        if (!getGameClient().canConnect()) return;
        String serverIP = JOptionPane.showInputDialog(null, "Server IP");
        boolean isConnectionSuccessful = getGameClient().connect(serverIP);
        if (isConnectionSuccessful) EventQueue.invokeLater(() -> new ConnectWindow(getGameClient()));
        else JOptionPane.showMessageDialog(null, "Server is not reachable!");
    }

}
