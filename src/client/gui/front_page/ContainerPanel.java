package client.gui.front_page;

import client.gui.ComponentProportion;
import client.gui.IComponent;
import client.gui.components.button.Button;
import client.gui.components.button.ButtonEventListener;
import client.gui.front_page.components.ConnectButton;
import client.gui.front_page.components.ExitButton;
import client.gui.front_page.components.SettingsButton;
import client.logic.GameClient;
import client.utility.Utility;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ContainerPanel extends JPanel implements IComponent {
    private final GameClient gameClient;
    private Button connectButton;
    private Button settingsButton;
    private Button exitButton;
    private final ComponentProportion centeringOffsetProportion = new ComponentProportion(230./600., 145./400);
    private final ComponentProportion commonButtonSizeProportion = new ComponentProportion(140./600., 30./400.);
    private final ComponentProportion commonButtonGapProportion = new ComponentProportion(ComponentProportion.BASE_PROPORTION, 10./400.);
    private final ButtonEventListener buttonEventListener = new ButtonEventListener();

    public ContainerPanel(GameClient gameClient) {
        this.gameClient = gameClient;
        setDefaults();
        addControls();
    }

    private void addControls() {
        connectButton = new ConnectButton(this, Utility.OffsetDirection.NONE, commonButtonSizeProportion, null, this, gameClient, buttonEventListener, Color.gray);
        settingsButton = new SettingsButton(connectButton, Utility.OffsetDirection.VERTICAL, commonButtonSizeProportion, commonButtonGapProportion, this, gameClient, buttonEventListener, Color.gray);
        exitButton = new ExitButton(settingsButton, Utility.OffsetDirection.VERTICAL, commonButtonSizeProportion, commonButtonGapProportion, this, gameClient, buttonEventListener, Color.gray);
    }

    private void setDefaults() {
        setLayout(new BorderLayout());
        setSize(new Dimension(614, 400)); //FIXME: valahogy a méretet függetleníteni az oprendszertől
        addMouseListener(mouseListener);
    }

    @Override
    public Point getOffset() {
        return Utility.convertDimensionToPoint(Utility.scaleWithProportion(getSize(), centeringOffsetProportion));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        connectButton.draw((Graphics2D) g);
        settingsButton.draw((Graphics2D) g);
        exitButton.draw((Graphics2D) g);
    }

    private final MouseListener mouseListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            super.mouseClicked(e);
            buttonEventListener.click(e.getPoint());
        }
    };
}
