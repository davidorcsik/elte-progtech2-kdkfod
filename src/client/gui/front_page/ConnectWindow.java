package client.gui.front_page;

import client.gui.Drawable;
import client.gui.IComponent;
import client.gui.MainFrame.MainFrame;
import client.logic.GameClient;

import javax.swing.*;
import java.awt.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class ConnectWindow extends JFrame implements IComponent, Drawable {
    private final GameClient gameClient;
    private final Object gameStartMonitor = new Object();

    public ConnectWindow(GameClient gameClient) {
        this.gameClient = gameClient;
        this.gameClient.setGameStartMonitor(gameStartMonitor);
        setLayout(new BorderLayout());
        setSize(400,400);
        setBackground(Color.white);
        setUndecorated(true);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        add(new ConnectPanel(this, gameClient));

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(screenSize.width/2-this.getSize().width/2, screenSize.height/2-this.getSize().height/2);
        setVisible(true);

        new Thread(this::checkGameState).start();
    }

    private void checkGameState() {
        synchronized (gameStartMonitor) {
            try { gameStartMonitor.wait(); }
            catch (InterruptedException ignored) {}
        }
        EventQueue.invokeLater(() -> new MainFrame(gameClient));
        close();
    }

    private void close() {
        dispose();
    }

    @Override
    public Point getOffset() {
        return new Point(0, 0);
    }

    @Override
    public void draw(Graphics2D g) {}
}

