package client.gui.MainFrame;

import client.gui.ComponentProportion;
import client.gui.IComponent;
import client.gui.MainFrame.components.*;
import client.gui.Component;
import client.logic.GameClient;
import client.utility.Utility;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainPanel extends JPanel implements IComponent {
    private final JFrame parent;
    private final GameClient gameClient;
    private final Component gameField;
    private final ComponentProportion gameFieldProportion = new ComponentProportion(ComponentProportion.BASE_PROPORTION, 27./49.);
    private final Component gameHUD;
    private final ComponentProportion gameHUDProportion = new ComponentProportion(ComponentProportion.BASE_PROPORTION, 22./49.);
    private final ScheduledExecutorService redrawTimer = Executors.newSingleThreadScheduledExecutor();

    public MainPanel(GameClient gameClient, JFrame parent) {
        this.parent = parent;
        this.gameClient = gameClient;
        setDefaults();
        gameField = new GameField(this, Utility.OffsetDirection.NONE, gameFieldProportion, null, this, gameClient);
        gameHUD = new GameHUD(gameField, Utility.OffsetDirection.VERTICAL, gameHUDProportion, null, this, gameClient);
        redrawTimer.scheduleAtFixedRate(this::repaint, 100, 100, TimeUnit.MILLISECONDS);
    }

    private void setDefaults() {
        setLayout(new BorderLayout());
        setSize(new Dimension(1500, 1000));
        setVisible(true);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (gameClient.isGameWon()) {
            //gameClient.close();
            parent.dispatchEvent(new WindowEvent(parent, WindowEvent.WINDOW_CLOSING));
        }
        Graphics2D graphics2D = (Graphics2D) g;
        gameField.draw(graphics2D);
        gameHUD.draw(graphics2D);
    }

    @Override
    public Point getOffset() { return new Point(0, 0); }
}
