package client.gui.MainFrame.components;

import client.gui.Component;
import client.gui.ComponentProportion;
import client.gui.Drawable;
import client.gui.IComponent;
import client.gui.components.image.ImageContainer;
import client.logic.GameClient;
import client.utility.DrawHelper;
import client.utility.EntitySpriteTranslator;
import client.utility.Utility;
import common.map.Direction;
import common.map.entity.player.Player;

import java.awt.*;

public class CharacterStatus extends Component implements Drawable {

    private final ComponentProportion pictureProportion = new ComponentProportion(50./200., 50./269.);
    private final ComponentProportion statusBarSizeProportion = new ComponentProportion(180./200., 20./269.);
    private final ComponentProportion pictureStatusBarGapProportion = new ComponentProportion(ComponentProportion.BASE_PROPORTION, 50./269.);
    private final ComponentProportion statusBarGapProportion = new ComponentProportion(ComponentProportion.BASE_PROPORTION, 20./269.);

    private final ImageContainer playerPictureContainer;
    private final StatusBar healthBar;
    private final StatusBar progressBar;

    public CharacterStatus(IComponent offsetFrom, Utility.OffsetDirection offsetDirection, ComponentProportion proportion, ComponentProportion gapProportion, IComponent parentComponent, GameClient gameClient) {
        super(offsetFrom, offsetDirection, proportion, gapProportion, parentComponent, gameClient);
        Image playerImage = EntitySpriteTranslator.getSpriteFromMapItem(getGameClient().getPlayer()).getSprite(new Dimension(100, 100), Direction.DOWN);
        playerPictureContainer = new ImageContainer(this, Utility.OffsetDirection.NONE, pictureProportion, null, this, gameClient, playerImage);

        healthBar = new StatusBar(playerPictureContainer, Utility.OffsetDirection.VERTICAL, statusBarSizeProportion, pictureStatusBarGapProportion, this, gameClient);
        healthBar.setBackgroundColor(Color.red);
        healthBar.setForegroundColor(Color.green);
        healthBar.setMaxValue(Player.MAX_HEALTH);

        progressBar = new StatusBar(healthBar, Utility.OffsetDirection.VERTICAL, statusBarSizeProportion, statusBarGapProportion, this, gameClient);
        progressBar.setForegroundColor(Color.blue);
        progressBar.setBackgroundColor(Color.gray);
        progressBar.setMaxValue(100);
    }

    private void updateMetrics() {
        healthBar.setCurrentValue(getGameClient().getPlayer().getHealthPoint());
        progressBar.setCurrentValue(getGameClient().getInteractionProgress());
    }

    @Override
    public void draw(Graphics2D g) {
        updateMetrics();

        playerPictureContainer.draw(g);
        healthBar.draw(g);
        progressBar.draw(g);
    }
}
