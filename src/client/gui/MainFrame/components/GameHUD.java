package client.gui.MainFrame.components;

import client.gui.Component;
import client.gui.ComponentProportion;
import client.gui.Drawable;
import client.gui.IComponent;
import client.logic.GameClient;
import client.utility.Utility;

import java.awt.*;

public class GameHUD extends Component implements Drawable {
    private final Component characterStatus;
    private final ComponentProportion characterStatusProportion = new ComponentProportion(1./4., ComponentProportion.BASE_PROPORTION);
    private final Component statistics;
    private final ComponentProportion statisticsProportion = new ComponentProportion(1./2., ComponentProportion.BASE_PROPORTION);
    private final Component miniMap;
    private final ComponentProportion miniMapProportion = new ComponentProportion(1./4., ComponentProportion.BASE_PROPORTION);

    public GameHUD(IComponent offsetFrom, Utility.OffsetDirection offsetDirection, ComponentProportion proportion, ComponentProportion gapProportion, IComponent parentComponent, GameClient gameClient) {
        super(offsetFrom, offsetDirection, proportion, gapProportion, parentComponent, gameClient);
        characterStatus = new CharacterStatus(this, Utility.OffsetDirection.NONE, characterStatusProportion, null, this, gameClient);
        statistics = new Statistics(characterStatus, Utility.OffsetDirection.HORIZONTAL, statisticsProportion, null, this, gameClient);
        miniMap = new MiniMap(statistics, Utility.OffsetDirection.HORIZONTAL, miniMapProportion, null, this, gameClient);
    }

    @Override
    public void draw(Graphics2D g) {
        characterStatus.draw(g);
        statistics.draw(g);
        miniMap.draw(g);
    }
}
