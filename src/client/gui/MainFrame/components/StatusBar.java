package client.gui.MainFrame.components;

import client.gui.Component;
import client.gui.ComponentProportion;
import client.gui.Drawable;
import client.gui.IComponent;
import client.logic.GameClient;
import client.utility.DrawHelper;
import client.utility.Utility;

import java.awt.*;

public class StatusBar extends Component implements Drawable {
    private int maxValue;
    private int currentValue;
    private Color foregroundColor;
    private Color backgroundColor;

    public StatusBar(IComponent offsetFrom, Utility.OffsetDirection offsetDirection, ComponentProportion proportion, ComponentProportion gapProportion, IComponent parentComponent, GameClient gameClient) {
        super(offsetFrom, offsetDirection, proportion, gapProportion, parentComponent, gameClient);
    }

    void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    void setCurrentValue(int currentValue) { this.currentValue = currentValue; }

    void setForegroundColor(Color foregroundColor) {
        this.foregroundColor = foregroundColor;
    }

    void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    @Override
    public void draw(Graphics2D g) {
        Point healthBarRemainingOffset = new Point(getOffset());
        Dimension healthBarRemainingSize = new Dimension(
                (int)Math.round((double)currentValue/maxValue*getSize().width),
                getSize().height);
        Point healthBarLostOffset = new Point(
                healthBarRemainingOffset.x + healthBarRemainingSize.width,
                healthBarRemainingOffset.y);
        Dimension healthBarLostSize = new Dimension(
                getSize().width - healthBarRemainingSize.width,
                healthBarRemainingSize.height);
        DrawHelper.drawFilledRect(g, healthBarRemainingOffset, healthBarRemainingSize, foregroundColor);
        DrawHelper.drawFilledRect(g, healthBarLostOffset, healthBarLostSize, backgroundColor);
    }
}
