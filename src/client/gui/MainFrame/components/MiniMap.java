package client.gui.MainFrame.components;

import client.gui.Component;
import client.gui.ComponentProportion;
import client.gui.Drawable;
import client.gui.IComponent;
import client.logic.GameClient;
import client.utility.Utility;
import common.map.entity.furniture.Furniture;
import common.map.entity.pickup.Pickup;
import common.map.entity.player.Player;
import common.map.field.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;

public class MiniMap extends Component implements Drawable {
    private static final HashMap<Class<?>, Color> fieldColorTranslationTable = new HashMap<>();
    static {
        fieldColorTranslationTable.put(Wall.class, Color.black);
        fieldColorTranslationTable.put(Robot.class, Color.red);
        fieldColorTranslationTable.put(Furniture.class, Color.green);
        fieldColorTranslationTable.put(Panel.class, Color.green);
        fieldColorTranslationTable.put(Elevator.class, Color.blue);
        fieldColorTranslationTable.put(Pickup.class, Color.yellow);
        fieldColorTranslationTable.put(Ground.class, Color.gray);
        fieldColorTranslationTable.put(Player.class, Color.orange);
    }

    private static Color getColorForField(Field field) {
        Class fieldClass;
        if (field.getContainedEntity() != null) fieldClass = field.getContainedEntity().getClass();
        else fieldClass = field.getClass();
        if (fieldColorTranslationTable.containsKey(fieldClass)) return fieldColorTranslationTable.get(fieldClass);
        return fieldColorTranslationTable.get(Ground.class);
    }

    public MiniMap(IComponent offsetFrom, Utility.OffsetDirection offsetDirection, ComponentProportion proportion, ComponentProportion gapProportion, IComponent parentComponent, GameClient gameClient) {
        super(offsetFrom, offsetDirection, proportion, gapProportion, parentComponent, gameClient);
    }

    @Override
    public void draw(Graphics2D g) {
        Image map = createMapImage().getScaledInstance(getSize().width, getSize().height, Image.SCALE_DEFAULT);
        g.drawImage(map, getOffset().x, getOffset().y, null);
    }

    private BufferedImage createMapImage() {
        Field[][] fields = getGameClient().getFields();
        BufferedImage image = new BufferedImage(fields.length, fields[0].length, BufferedImage.TYPE_INT_BGR);
        for (int i = 0; i<fields.length; ++i) {
            for (int j = 0; j<fields[i].length; ++j) { image.setRGB(i, j, getColorForField(fields[i][j]).getRGB()); }
        }

        return image;
    }
}
