package client.gui.MainFrame.components;

import client.gui.Component;
import client.gui.ComponentProportion;
import client.gui.Drawable;
import client.gui.IComponent;
import client.logic.GameClient;
import client.utility.EntitySpriteTranslator;
import client.utility.Sprite;
import client.utility.Utility;
import common.map.Map;
import common.map.entity.Entity;
import common.map.field.Field;

import java.awt.*;

public class GameField extends Component implements Drawable {
    private Dimension drawDistance;
    public GameField(IComponent offsetFrom, Utility.OffsetDirection offsetDirection, ComponentProportion proportion, ComponentProportion gapProportion, IComponent parentComponent, GameClient gameClient) {
        super(offsetFrom, offsetDirection, proportion, gapProportion, parentComponent, gameClient);
        drawDistance = new Dimension(20, 7);
    }

    public void draw(Graphics2D g) {
        Field[][] fields = Map.getFieldsAroundCoordinate(getGameClient().getFields(), getGameClient().getMap().getPlayerWithID(getGameClient().getUID()).getLocation(), drawDistance);

        Dimension fieldSize = Utility.scaleWithProportion(getSize(), new ComponentProportion(1.0/fields.length, 1.0/fields[0].length));

        for (int i = 0 ; i < fields.length ; i++) {
            for (int j = 0 ; j < fields[i].length ; j++) {
                Point fieldOffset = Utility.scaleWithProportion(new Point(i, j), new Dimension(fieldSize.width, fieldSize.height));
                g.drawImage(EntitySpriteTranslator.getSpriteFromMapItem(fields[i][j]).getSprite(fieldSize, null), fieldOffset.x, fieldOffset.y, null);

                if (fields[i][j].getContainedEntity() != null) {
                    Entity entity = fields[i][j].getContainedEntity();
                    Sprite entitySprite = EntitySpriteTranslator.getSpriteFromMapItem(entity);
                    g.drawImage(entitySprite.getSprite(fieldSize, entity.getFacingDirection()), fieldOffset.x, fieldOffset.y, null);
                }
            }
        }
    }
}
