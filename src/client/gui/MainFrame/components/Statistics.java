package client.gui.MainFrame.components;

import client.gui.Component;
import client.gui.ComponentProportion;
import client.gui.Drawable;
import client.gui.IComponent;
import client.logic.GameClient;
import client.utility.DrawHelper;
import client.utility.ImageLoader;
import client.utility.Utility;
import common.map.entity.player.InventoryItem;
import common.map.entity.player.Player;
import common.utility.FilePathConstants;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;

import static client.logic.GameClient.ITEM_PER_LEVEL;
import static client.logic.GameClient.LEVEL_COUNT;

public class Statistics extends Component implements Drawable {
    private Image ammoItemImage;
    private final ComponentProportion inventorySizeProportion = new ComponentProportion(740./750., 100./450.);
    private final ComponentProportion inventoryItemSizeProportion = new ComponentProportion(1./ LEVEL_COUNT, ComponentProportion.BASE_PROPORTION);
    private final ComponentProportion ammoDisplaySizeProportion = new ComponentProportion(740./750., 100./450.);
    private final ComponentProportion ammoItemSizeProportion = new ComponentProportion(1./ Player.MAX_AMMO, ComponentProportion.BASE_PROPORTION);
    private final ComponentProportion gapProportion = new ComponentProportion(ComponentProportion.BASE_PROPORTION, 100./450);
    private final ComponentProportion timerSizeProportion = new ComponentProportion(740./750., 100./800.);

    public Statistics(IComponent offsetFrom, Utility.OffsetDirection offsetDirection, ComponentProportion proportion, ComponentProportion gapProportion, IComponent parentComponent, GameClient gameClient) {
        super(offsetFrom, offsetDirection, proportion, gapProportion, parentComponent, gameClient);
        ammoItemImage = ImageLoader.getResourceImage(FilePathConstants.IMAGES_PATH + "ammo.png");
    }

    private BufferedImage getLetterImage(char letter, boolean found) {
        letter = Character.toLowerCase(letter);
        if (found) return (BufferedImage) ImageLoader.getResourceImage(FilePathConstants.INVENTORY_LETTER_PATH + letter + ".png");
        return (BufferedImage) ImageLoader.getResourceImage(FilePathConstants.INVENTORY_LETTER_PATH + letter + "_missing.png");
    }

    private Image getInventoryImage(List<InventoryItem> inventory, int level) {
        double foundSegmentPercent = ((double)inventory.stream().filter(item -> item.getLevel()==level).count()) / ITEM_PER_LEVEL;
        if (foundSegmentPercent == 0) return ImageLoader.getResourceImage(FilePathConstants.IMAGES_PATH + "inventory_slot_empty.png");

        char letter = inventory.stream().filter(item -> item.getLevel() == level).findFirst().orElse(null).getCharacter();
        if (foundSegmentPercent == 1) return getLetterImage(letter, true);

        BufferedImage inventoryImageFound = getLetterImage(letter, true);
        int foundImageHeight = (int) (inventoryImageFound.getHeight()*foundSegmentPercent);
        int foundImageStartY = inventoryImageFound.getHeight() - foundImageHeight;
        BufferedImage inventoryImageFoundCut = inventoryImageFound.getSubimage(0, foundImageStartY, inventoryImageFound.getWidth(), foundImageHeight);

        BufferedImage inventoryImageMissing = getLetterImage(letter, false);
        int missingImageHeight = inventoryImageMissing.getHeight() - foundImageHeight;
        BufferedImage inventoryImageMissingCut = inventoryImageMissing.getSubimage(0, 0, inventoryImageMissing.getWidth(), missingImageHeight);

        BufferedImage inventoryImage = new BufferedImage(inventoryImageFound.getWidth(), inventoryImageFound.getHeight(), inventoryImageFound.getType());
        Graphics2D inventoryImageGraphics = (Graphics2D) inventoryImage.getGraphics();
        inventoryImageGraphics.drawImage(inventoryImageFoundCut, 0, foundImageStartY, null);
        inventoryImageGraphics.drawImage(inventoryImageMissingCut, 0, 0, null);
        inventoryImageGraphics.dispose();
        return inventoryImage;
    }

    private void drawInventory(Graphics2D g) {
        Dimension inventorySize = Utility.scaleWithProportion(getSize(), inventorySizeProportion);
        Dimension inventoryItemSize = Utility.scaleWithProportion(inventorySize, inventoryItemSizeProportion);
        Point inventoryOffset = new Point(Utility.calculateCenterOffset(getOffset(), getSize(), inventorySize).x, getOffset().y);
        for(int i = 0; i < LEVEL_COUNT; i++) {
            Point inventoryItemOffset = Utility.getPointWithOffset(inventoryOffset, Utility.scaleWithProportion(inventoryItemSize, new ComponentProportion(i, 0)), Utility.OffsetDirection.HORIZONTAL);
            Image inventoryPicture = getInventoryImage(getGameClient().getPlayer().getInventory(), i+1);
            DrawHelper.drawImage(g, inventoryPicture, inventoryItemOffset, inventoryItemSize);
        }
    }

    private void drawAmmoData(Graphics2D g) {
        Dimension ammoDisplaySize = Utility.scaleWithProportion(getSize(), ammoDisplaySizeProportion);
        Dimension ammoItemSize = Utility.scaleWithProportion(ammoDisplaySize, ammoItemSizeProportion);
        int offsetY = Utility.scaleWithProportion(getSize(), ComponentProportion.sumProportions(inventorySizeProportion, gapProportion)).height;
        int offsetX = Utility.calculateCenterOffset(getSize(), ammoDisplaySize).x;
        Point offset = Utility.getPointWithOffset(new Point(offsetX, offsetY), getOffset());
        for (int i = 0; i<getGameClient().getPlayer().getAmmoAmount(); ++i) {
            DrawHelper.drawImage(g, ammoItemImage, Utility.getPointWithOffset(offset, new Point(i * ammoItemSize.width/2, 0)), ammoItemSize);
        }
    }

    private void drawTimer(Graphics2D g) {
        Dimension timerSize = Utility.scaleWithProportion(getSize(), timerSizeProportion);
        int offsetY = Utility.scaleWithProportion(getSize(),
                        ComponentProportion.sumProportions(
                            ComponentProportion.sumProportions(
                                ComponentProportion.sumProportions(inventorySizeProportion, gapProportion), ammoDisplaySizeProportion), gapProportion)).height;
        int offsetX = Utility.calculateCenterOffset(getSize(), timerSize).x;
        Point offset = Utility.getPointWithOffset(new Point(offsetX, offsetY), getOffset());
        int timeLeftWith = (int)Math.round(timerSize.width * ((double)getGameClient().getTimeLeft()/getGameClient().getGameTime()));
        DrawHelper.drawFilledRect(g, offset, timerSize, Color.RED);
        DrawHelper.drawFilledRect(g, offset, new Dimension(timeLeftWith, timerSize.height), Color.GREEN);
    }

    @Override
    public void draw(Graphics2D g) {
        DrawHelper.drawFilledRect(g, getOffset(), getSize(), Color.ORANGE);
        drawInventory(g);
        drawAmmoData(g);
        drawTimer(g);
    }
}
