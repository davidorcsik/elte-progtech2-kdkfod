package client.gui.MainFrame;

import client.logic.GameClient;
import client.utility.KeyboardCommand;
import common.map.entity.player.ActionType;
import common.map.entity.panel.Panel;
import common.map.field.Field;
import common.utility.Utility;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainFrame extends JFrame implements WindowListener {
    private final GameClient gameClient;
    private final MainPanel mainPanel;
    private final ScheduledExecutorService keyPressExecutor = Executors.newSingleThreadScheduledExecutor();
    private Future<?> keyPressExecutorFuture;

    public MainFrame(GameClient gameClient) {
        this.gameClient = gameClient;
        this.mainPanel = new MainPanel(gameClient, this);
        setDefaults();
        addControls();
    }

    private void addControls() {
        add(mainPanel);
    }

    private void setDefaults() {
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        addWindowListener(this);
        addKeyListener(keyListener);
        setLayout(new BorderLayout());
        setSize(new Dimension(1514, 1000));
        setVisible(true);
    }

    private final KeyListener keyListener = new KeyListener() {
        @Override
        public void keyTyped(KeyEvent keyEvent) {}
        @Override
        public void keyPressed(KeyEvent keyEvent) {
            KeyboardCommand command = KeyboardCommand.getKeyboardCommandFromKeyCode(keyEvent.getKeyCode());
            if (command == null || gameClient.isInteractionInProgress()) return;
            if (command.actionType.equals(ActionType.INTERACT)) {
                Point nextPositionInFacingDirection = Utility.getPointInDirection(gameClient.getPlayer().getLocation(), gameClient.getPlayer().getFacingDirection());
                Field nextFieldInFacingDirection = gameClient.getMap().getField(nextPositionInFacingDirection);
                if (nextFieldInFacingDirection != null && nextFieldInFacingDirection.getContainedEntity() instanceof Panel && gameClient.hasPlayerGotAllLetters())  {
                    String providedDeactivationCode = JOptionPane.showInputDialog("Nuclear terminal\nNuclear launch system is active. To initiate shutdown provide deactivation code!");
                    gameClient.sendDeactivationCode(providedDeactivationCode);
                } else {
                    keyPressExecutorFuture = keyPressExecutor.scheduleAtFixedRate(gameClient::incrementInteractionProgress,
                            0, gameClient.getTimeBetweenInteractionProgressIncrements(), TimeUnit.MILLISECONDS);
                    gameClient.setInteractionInProgress(true);
                }
            }
        }
        @Override
        public void keyReleased(KeyEvent keyEvent) {
            KeyboardCommand command = KeyboardCommand.getKeyboardCommandFromKeyCode(keyEvent.getKeyCode());
            if (command == null) return;
            if (command.actionType == ActionType.INTERACT) {
                keyPressExecutorFuture.cancel(true);
                gameClient.setInteractionInProgress(false);
                gameClient.setInteractionProgress(0);
            } else if (!gameClient.isInteractionInProgress()) {
                Point position = gameClient.getPlayer().getLocation();
                if (command.actionType == ActionType.MOVE) position = Utility.getPointInDirection(position, command.direction);
                gameClient.sendIntent(position, command.actionType);
            }
        }
    };

    @Override
    public void windowOpened(WindowEvent e) {}

    @Override
    public void windowClosing(WindowEvent e) {}

    @Override
    public void windowClosed(WindowEvent e) {}

    @Override
    public void windowIconified(WindowEvent e) {}

    @Override
    public void windowDeiconified(WindowEvent e) {}

    @Override
    public void windowActivated(WindowEvent e) {}

    @Override
    public void windowDeactivated(WindowEvent e) {}
}
