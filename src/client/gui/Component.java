package client.gui;

import client.logic.GameClient;
import client.utility.Utility;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;

public abstract class Component implements IComponent, Drawable {
    private final IComponent offsetFrom;
    private final Utility.OffsetDirection offsetDirection;
    private final ComponentProportion proportion;
    private final ComponentProportion gapProportion;
    private final IComponent parentComponent;
    private final GameClient gameClient;

    protected Component(IComponent offsetFrom, Utility.OffsetDirection offsetDirection, ComponentProportion proportion, ComponentProportion gapProportion, IComponent parentComponent, GameClient gameClient) {
        this.offsetFrom = offsetFrom;
        this.offsetDirection = offsetDirection;
        this.parentComponent = parentComponent;
        this.gapProportion = gapProportion;
        this.proportion = proportion;
        this.gameClient = gameClient;
    }

    public Point getOffset() {
        Point offset = Utility.getPointWithOffset(offsetFrom.getOffset(), offsetFrom.getSize(), offsetDirection);
        if (gapProportion == null) return offset;
        Dimension gapOffset = Utility.scaleWithProportion(parentComponent.getSize(), gapProportion);
        return Utility.getPointWithOffset(offset, gapOffset, offsetDirection);
    }

    public Dimension getSize() {
        return Utility.calculateDimensionByProportion(parentComponent.getSize(), proportion);
    }

    public IComponent getContainer() { return parentComponent; }

    public GameClient getGameClient() { return gameClient; }
}
