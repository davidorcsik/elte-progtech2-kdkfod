package client.gui;

import java.awt.*;

public interface IComponent {
    Point getOffset();
    Dimension getSize();
}
