package client.gui;

import java.awt.*;

public class ComponentProportion extends Dimension {
    public static final double BASE_PROPORTION = 1.;
    private final double widthProportion;
    private final double heightProportion;

    public ComponentProportion(double widthProportion, double heightProportion) {
        this.widthProportion = widthProportion;
        this.heightProportion = heightProportion;
    }

    public double getWidthProportion() {
        return widthProportion;
    }

    public double getHeightProportion() { return heightProportion; }

    public static ComponentProportion sumProportions(ComponentProportion p1, ComponentProportion p2) {
        return new ComponentProportion(p1.widthProportion + p2.widthProportion, p1.heightProportion + p2.heightProportion);
    }
}
