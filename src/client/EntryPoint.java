package client;

import client.gui.MainFrame.MainFrame;
import client.gui.front_page.ConnectWindow;
import client.gui.front_page.FrontPage;
import client.logic.GameClient;

import java.awt.*;

public class EntryPoint {
    public static void main(String[] args) {
        GameClient logic = new GameClient();
        EventQueue.invokeLater(() -> new FrontPage(logic));
    }
}
