package client.utility;

import common.map.entity.Entity;
import common.map.entity.Projectile;
import common.map.entity.furniture.Furniture;
import common.map.entity.panel.Panel;
import common.map.entity.pickup.Pickup;
import common.map.entity.furniture.FurnitureType;
import common.map.entity.player.Player;
import common.map.entity.robot.Robot;
import common.map.entity.robot.RobotBehaviourAggressive;
import common.map.field.Elevator;
import common.map.field.Ground;
import common.map.field.Spawn;
import common.map.field.Wall;
import common.map.entity.robot.Robot;
import common.map.entity.robot.RobotType;

import java.util.Arrays;

public class EntitySpriteTranslator {
    private static Object[][] translationTable = {
            {Player.class, Sprite.PLAYER_ONE},
            {Player.class, Sprite.PLAYER_TWO},
            {Furniture.class,  Sprite.FURNITURE_TABLE},
            {Furniture.class,  Sprite.FURNITURE_CUPBOARD},
            {Furniture.class,  Sprite.FURNITURE_RACK},
            {Furniture.class,  Sprite.FURNITURE_SHELF},
            {Robot.class, Sprite.ROBOT_TACTICAL},
            {Robot.class, Sprite.ROBOT_AGGRESSIVE},
            {Elevator.class, Sprite.ELEVATOR},
            {Spawn.class, Sprite.SPAWN_POINT},
            {Pickup.class, Sprite.PICKUP_HP},
            {Pickup.class, Sprite.PICKUP_AMMO},
            {Panel.class, Sprite.PANEL},
            {Ground.class, Sprite.GROUND},
            {Wall.class, Sprite.WALL},
            {Projectile.class, Sprite.PROJECTILE}
    };

    /** Translates map item to sprite */
    public static Sprite getSpriteFromMapItem(Object item) {
        int translationOffset = 0;
        if      (item.getClass().equals(Player.class))      translationOffset = (int) (((Player)item).getId() & 0b1);
        else if (item.getClass().equals(Robot.class))       translationOffset = ((Robot)item).getType().ordinal();
        else if (item.getClass().equals(Furniture.class))   translationOffset = ((Furniture)item).getType().ordinal();
        else if (item.getClass().equals(Pickup.class))      translationOffset = ((Pickup)item).getType().ordinal();

        int i = 0;
        while (i < translationTable.length && !translationTable[i][0].equals(item.getClass())) ++i;
        if (i < translationTable.length) return (Sprite) translationTable[i + translationOffset][1];
        return null;
    }
}
