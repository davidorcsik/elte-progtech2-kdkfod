package client.utility;

import client.gui.ComponentProportion;
import common.map.Direction;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.Arrays;

public class Utility {
    public enum OffsetDirection {
        HORIZONTAL(1, 0), VERTICAL(0, 1), BOTH(1, 1), NONE(0, 0)
        ;
        private final int dx, dy;

        OffsetDirection(int dx, int dy) {
            this.dx = dx;
            this.dy = dy;
        }

        public int getDx() {
            return dx;
        }

        public int getDy() {
            return dy;
        }
    }

    public static Point getPointWithOffset(Point origin, Point offset) {
        return getPointWithOffset(origin, offset, OffsetDirection.BOTH);
    }

    public static Point getPointWithOffset(Point origin, Dimension offset) {
        return getPointWithOffset(origin, offset, OffsetDirection.BOTH);
    }

    public static Point getPointWithOffset(Point origin, Dimension offset, OffsetDirection offsetDirection) {
        return getPointWithOffset(origin, new Point(offset.width, offset.height), offsetDirection);
    }

    public static Point getPointWithOffset(Point origin, Point offset, OffsetDirection offsetDirection) {
        Point offsetPoint = new Point(origin);
        offsetPoint.translate(offset.x*offsetDirection.getDx(), offset.y*offsetDirection.getDy());
        return offsetPoint;
    }

    public static Point scaleWithProportion(Point origin, Point offset) {
        return new Point(origin.x * offset.x, origin.y * offset.y);
    }

    public static Dimension scaleWithProportion(Dimension origin, ComponentProportion proportion) {
        return new Dimension((int)Math.round(origin.width * proportion.getWidthProportion()), (int)Math.round(origin.height * proportion.getHeightProportion()));
    }

    public static Point scaleWithProportion(Point origin, ComponentProportion proportion) {
        return new Point((int)Math.round(origin.x * proportion.getWidthProportion()), (int)Math.round(origin.y * proportion.getHeightProportion()));
    }

    public static Point scaleWithProportion(Point origin, Dimension proportion) {
        return new Point(Math.round(origin.x * proportion.width), Math.round(origin.y * proportion.height));
    }

    public static Point convertDimensionToPoint(Dimension dimension) {
        return new Point(dimension.width, dimension.height);
    }

    public static Dimension convertPointToDimension(Point point) {
        return new Dimension(point.x, point.y);
    }

    public static Point calculateCenterOffset(Dimension containerSize, Dimension componentSize) {
        return new Point((containerSize.width - componentSize.width)/2, (containerSize.height - componentSize.height)/2);
    }

    public static Point calculateCenterOffset(Point containerOffset, Dimension containerSize, Dimension componentSize) {
        Point centerOffset = calculateCenterOffset(containerSize, componentSize);
        centerOffset.translate(containerOffset.x, containerOffset.y);
        return centerOffset;
    }

    public static Dimension calculateDimensionByProportion(Dimension containerSize, ComponentProportion proportion) {
        return new Dimension(
                (int)Math.round(containerSize.width*proportion.getWidthProportion()),
                (int)Math.round(containerSize.height*proportion.getHeightProportion()));
    }

    public static Point calculatePointByProportion(Point origin, ComponentProportion proportion) {
        return new Point(
                (int)Math.round(origin.x*proportion.getWidthProportion()),
                (int)Math.round(origin.y*proportion.getHeightProportion()));
    }

    public static boolean between(int value, int minValue, int maxValue) {
        return value >= minValue && value <= maxValue;
    }

    public static boolean isPointInRect(Rectangle rect, Point point) {
        return between(point.x, rect.x, rect.x + rect.width) && between(point.y, rect.y, rect.y + rect.height);
    }

    public static BufferedImage copyImage(BufferedImage image) {
        ColorModel cm = image.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = image.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    public static Image rotateImage(BufferedImage image, double rotation) {
        BufferedImage rotatedImage = copyImage(image);
        AffineTransform tx = new AffineTransform();
        tx.rotate(rotation, rotatedImage.getWidth()/2, rotatedImage.getHeight()/2);
        return new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR).filter(rotatedImage, null);
    }
}

