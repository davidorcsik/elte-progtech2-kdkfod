package client.utility;

import common.utility.FilePathConstants;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class ImageLoader {
    public static Image getResourceImageWithSize(String name, int width, int height) {
        return getResourceImage(name).getScaledInstance(width, height, Image.SCALE_SMOOTH);
    }

    public static Image getResourceImageIconWithSize(String name, int width, int height) {
        return getResourceImageIcon(name).getScaledInstance(width, height, Image.SCALE_SMOOTH);
    }

    public static Image getResourceImage(String filePath) {
        try { return ImageIO.read(new File(filePath)); }
        catch (IOException e) { e.printStackTrace(); }
        return null;
    }

    public static Image getResourceImageIcon(String name) {
        return new ImageIcon(new File(name).getAbsolutePath()).getImage();
    }
}
