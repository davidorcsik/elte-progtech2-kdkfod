package client.utility;

import common.map.Direction;
import common.map.entity.player.ActionType;

import java.util.Arrays;

public enum KeyboardCommand {
    W(87, ActionType.MOVE, Direction.UP),
    A(65, ActionType.MOVE, Direction.LEFT),
    S(83, ActionType.MOVE, Direction.DOWN),
    D(68, ActionType.MOVE, Direction.RIGHT),
    E(69, ActionType.INTERACT),
    CTRL(17, ActionType.ATTACK),
    ;

    public final int keyCode;
    public final ActionType actionType;
    public final Direction direction;

    KeyboardCommand(int keyCode, ActionType actionType, Direction direction) {
        this.keyCode = keyCode;
        this.actionType = actionType;
        this.direction = direction;
    }

    KeyboardCommand(int keyCode, ActionType actionType) { this(keyCode, actionType, null); }

    public static KeyboardCommand getKeyboardCommandFromKeyCode(int keyCode) {
        return Arrays.stream(KeyboardCommand.values()).filter(k -> k.keyCode == keyCode).findFirst().orElse(null);
    }
}
