package client.utility;

import java.awt.*;

public class DrawHelper {
    public static void drawFilledRect(Graphics2D g, Point position, Dimension size, Color color) {
        Color temp = g.getColor();
        g.setColor(color);
        g.fillRect(position.x, position.y, size.width, size.height);
        g.setColor(temp);
    }

    public static void drawCenteredString(Graphics2D g, String text, Rectangle rect, Font font, Color color) {
        Font tempFont = g.getFont();
        Color tempColor = g.getColor();
        FontMetrics metrics = g.getFontMetrics(font);
        int x = rect.x + (rect.width - metrics.stringWidth(text)) / 2;
        int y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent();
        g.setColor(color);
        g.setFont(font);
        g.drawString(text, x, y);
        g.setColor(tempColor);
        g.setFont(tempFont);
    }

    public static void drawImage(Graphics2D g, Image image, Point position, Dimension size) {
        g.drawImage(image, position.x, position.y, size.width, size.height, null);
    }
}
