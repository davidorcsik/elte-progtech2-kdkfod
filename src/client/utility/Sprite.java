package client.utility;

import common.map.Direction;
import common.utility.FilePathConstants;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

public enum Sprite {
    PLAYER_ONE("player_one.png", true),
    PLAYER_TWO("player_two.png", true),
    ROBOT_AGGRESSIVE("robot_aggressive.png", true),
    ROBOT_TACTICAL("robot_tactical.png", true),
    FURNITURE_CUPBOARD("furniture_cupboard.png", false),
    FURNITURE_RACK("furniture_rack.png", false),
    FURNITURE_SHELF("furniture_shelf.png", false),
    FURNITURE_TABLE("furniture_table.png", false),
    ELEVATOR("elevator_in.png", false),
    SPAWN_POINT("elevator_out_and_spawn_point.png", false),
    PICKUP_AMMO("pickup_ammo.png", false),
    PICKUP_HP("pickup_hp.png", false),
    PANEL("panel.png", false),
    GROUND("ground.png", false),
    WALL("wall.png", false),
    PROJECTILE("projectile.png", true);

    private final HashMap<Direction, Image> sprites = new HashMap<>();

    Sprite(String fileName, boolean multiDirectional) {
        try {
            if (multiDirectional) readMultidimensionalImage(fileName);
            else readMonoDirectionalSprite(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void readMultidimensionalImage(String fileName) throws IOException {
        int fileExtensionSeparatorIndex = fileName.lastIndexOf('.');
        String fileNameBase = fileName.substring(0, fileExtensionSeparatorIndex);
        String fileExtensionWithSeparator = fileName.substring(fileExtensionSeparatorIndex);

        boolean isFilesPresent = Arrays.stream(Direction.values()).anyMatch(d -> new File(FilePathConstants.SPRITES_PATH + fileNameBase + "_" + d.name().toUpperCase() + fileExtensionWithSeparator).exists());
        if (isFilesPresent) {
            for (Direction direction : Direction.values()) {
                sprites.put(direction, ImageLoader.getResourceImage(FilePathConstants.SPRITES_PATH + fileNameBase + "_" + direction.name().toUpperCase() + fileExtensionWithSeparator));
            }
        } else {
            BufferedImage baseImage = (BufferedImage) ImageLoader.getResourceImage(FilePathConstants.SPRITES_PATH + fileName);
            for (Direction direction : Direction.values()) {
                sprites.put(direction, Utility.rotateImage(baseImage, direction.getRotation()));
            }
        }
    }

    private void readMonoDirectionalSprite(String fileName) throws IOException {
        Image sprite = ImageLoader.getResourceImage(FilePathConstants.SPRITES_PATH + fileName);
        Arrays.stream(Direction.values()).forEach(d -> sprites.put(d, sprite));
    }

    public Image getSprite(Dimension size, Direction direction) { return getSprite(size.width, size.height, direction); }
    public Image getSprite(int width, int height, Direction direction) {
        if (direction == null) direction = Direction.UP;
        return sprites.get(direction).getScaledInstance(width, height, Image.SCALE_DEFAULT);
    }
}
