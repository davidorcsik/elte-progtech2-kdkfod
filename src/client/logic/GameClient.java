package client.logic;

import client.logic.worker.InputWorker;
import common.map.Map;
import common.map.entity.player.ActionType;
import common.map.entity.player.Player;
import common.map.field.Field;
import common.utility.Message;
import common.utility.MessageType;
import common.utility.Utility;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GameClient {
    public static final int LEVEL_COUNT = 9;
    public static final int ITEM_PER_LEVEL = 4;
    private final static int INTERACTION_TIME = 2500;

    private InputWorker inputWorker;
    private long UID;
    private Socket socket;
    private ObjectOutputStream socketWriter;
    private ObjectInputStream socketReader;
    private boolean gameStarted = false;
    private boolean gameWon = false;
    private Map map;
    private long gameTime;
    private long timeLeft;
    private boolean interactionInProgress;
    private int interactionProgress;
    private Object gameStartMonitor;
    private ScheduledExecutorService timer;

    public boolean connect(String serverIP) {
        if (inputWorker != null) inputWorker.close();
        timer = Executors.newSingleThreadScheduledExecutor();
        inputWorker = new InputWorker(this);
        try {
            socket = new Socket(serverIP, ConnectionData.SERVER_PORT);
            socketWriter = new ObjectOutputStream(socket.getOutputStream());
            socketReader = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            return false;
        }
        inputWorker.start();
        return true;
    }

    @SuppressWarnings("SynchronizeOnNonFinalField")
    public void startGame() {
        gameStarted = true;
        timer.scheduleAtFixedRate(() -> --timeLeft, 500, 1000, TimeUnit.MILLISECONDS);
        synchronized (gameStartMonitor) { gameStartMonitor.notifyAll(); }
    }

    public boolean isGameStarted() { return gameStarted; }

    public void setUID(long uid) { this.UID = uid; }

    public void close() {
        gameStarted = false;
        timer.shutdownNow();
        Utility.closeClosable(inputWorker);
        Utility.closeSocket(socket);
        Utility.closeClosable(socketReader);
        Utility.closeClosable(socketWriter);
        inputWorker = null;
        socket = null;
        socketReader = null;
        socketWriter = null;
    }

    public synchronized Message readMessage() throws IOException, ClassNotFoundException {
        try { return (Message) socketReader.readObject(); }
        catch (NullPointerException ignored) {} //socket zárás esetén
        return null;
    }

    public void sendIntent(Point newPosition, ActionType actionType) {
        System.out.println("newPosition = [" + newPosition + "], actionType = [" + actionType + "]");
        sendMessage(Message.createMessageFromIntent(newPosition, actionType, UID));
    }

    public Field[][] getFields() {
        return map.getFields();
    }

    @SuppressWarnings("SynchronizeOnNonFinalField")
    private void sendMessage(Message message) {
        try {
            synchronized (socketWriter) {
                socketWriter.writeObject(message);
                socketWriter.reset();
            }
        }
        catch (IOException e) { close(); }
        catch (NullPointerException ignored) {} //socket zárás esetén
    }

    public void exit() {
        System.exit(0);
    }

    public void updateGame(Map differenceMap) {
        map = Map.calculateNewStateFromDifferenceMap(map, differenceMap);
        if (timeLeft == 0) {
            JOptionPane.showMessageDialog(null, "Game Over!\nMisson not completed in time!");
            close();
        }
    }

    public Map getMap() {
        return map;
    }

    public long getUID() {
        return UID;
    }

    public Player getPlayer() {
        return map.getPlayerWithID(UID);
    }

    public long getGameTime() {
        return gameTime;
    }

    public long getTimeLeft() {
        return timeLeft;
    }

    public void setGameTime(long gameTime) {
        this.gameTime = gameTime;
        timeLeft = gameTime;
    }

    public void handleHandshakeData(long[] content) {
        setUID(content[0]);
        setGameTime(content[1]);
    }

    public void setTimeLeft(int timeLeft) {
        this.timeLeft = timeLeft;
    }

    public boolean isInteractionInProgress() {
        return interactionInProgress;
    }

    public void setInteractionInProgress(boolean interactionInProgress) {
        this.interactionInProgress = interactionInProgress;
    }

    public int getInteractionProgress() {
        return interactionProgress;
    }

    public void setInteractionProgress(int interactionProgress) {
        this.interactionProgress = interactionProgress;
    }

    public void incrementInteractionProgress() {
        ++interactionProgress;
        if (interactionProgress >= 100) {
            sendIntent(getPlayer().getLocation(), ActionType.INTERACT);
            setInteractionProgress(0);
        }
    }

    public int getTimeBetweenInteractionProgressIncrements() {
        return INTERACTION_TIME / 100;
    }

    public void setGameStartMonitor(Object gameStartMonitor) { this.gameStartMonitor = gameStartMonitor; }

    public void sendDeactivationCode(String providedDeactivationCode) {
        sendMessage(Message.createDeactivationCodeMessage(getUID(), providedDeactivationCode));
    }

    public void win() {
        System.out.println("WIN");
        gameWon = true;
        JOptionPane.showMessageDialog(null, "System disabled, nuclear launch diverted!");
    }

    public boolean isGameWon() {
        return gameWon;
    }

    public boolean hasPlayerGotAllLetters() {
        return getPlayer().getInventory().size() == LEVEL_COUNT * ITEM_PER_LEVEL;
    }

    public boolean canConnect() {
        return socket == null;
    }

    public void exitLobby() {
        sendMessage(new Message<>(MessageType.ABORT, null));
        close();
    }
}
