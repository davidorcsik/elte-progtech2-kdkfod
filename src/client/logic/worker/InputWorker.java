package client.logic.worker;

import client.logic.GameClient;
import common.map.Map;
import common.utility.Message;

import java.io.IOException;

public class InputWorker extends Thread implements AutoCloseable {
    private final GameClient gameClient;
    private boolean working = true;

    public InputWorker(GameClient gameClient) { this.gameClient = gameClient; }

    @Override
    public void run() {
        Message receivedMessage;
        try {
            while ((receivedMessage = gameClient.readMessage()) != null && working) {
                switch (receivedMessage.getType()) {
                    case HANDSHAKE: gameClient.handleHandshakeData((long[])receivedMessage.getContent()); break;
                    case ABORT: gameClient.close(); break;
                    case START: gameClient.updateGame((Map) receivedMessage.getContent()); gameClient.startGame(); break;
                    case STATE_CHANGE: gameClient.updateGame((Map) receivedMessage.getContent()); break;
                    case GAME_OVER: gameClient.setTimeLeft(0);
                    case WIN: gameClient.win();
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            gameClient.close();
        }
    }

    @Override
    public void close() {
        working = false;
    }
}
